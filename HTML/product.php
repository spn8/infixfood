<?php include 'include/header.php' ?>
<?php include 'include/menu2.php' ?>

<!-- banner::start  -->
<div class="banner_two">
    <div class="banner_single banner_bg_1">
        <div class="container">
            <div class="row">
                <div class="col-xl-7">
                    <div class="banner__text style2">
                        <span class="f_w_500 text-uppercase">Explore the whole cities</span>
                        <h3>Food delivery from 
                        Dhaka’s best Restaurants.
                        </h3>
                        <div class="banner_btns d-flex flex-wrap gap_10 align-items-center">
                            <div class="input-group theme_search_field style2">
                                <input type="text" class="form-control border-end-0" placeholder="Enter your full address" aria-label="Enter your full address" aria-describedby="basic-addon2">
                                <span class="input-group-text border-start-0" id="basic-addon2"><img src="img/svgs/loc_icon.svg" alt=""></span>
                            </div>
                            <button class="theme_btn shadow_btn">Find your favorites</button>
                        </div>
                        <p>Get the app and choose from 10,000+ restaurants in 70+ cities.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--/ banner::end  -->

<!-- infix_popular_area::start  -->
<div class="infix_section section_spacing2">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section__title mb_55 text-center">
                    <span class="sub_heading ">Best food in the city</span>
                    <h3 class="heading">Popular Restaurants</h3>
                </div>
            </div>
        </div>
        <div class="row mb_50">
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                <div class="product_wized mb_50">
                    <div class="thumb position-relative">
                        <a href="product_details.php" class="d-block">
                            <img src="img/product/product1.jpg" alt="">
                        </a>
                        <span class="product_tag position-absolute">15% OFF</span>
                    </div>
                    <div class="product_meta">
                        <h3><a href="product_details.php">Kosturi Kitchen Gulshan</a></h3>
                        <p> <span class="mark_text">$$$ </span>  Sushi <span class="dot_devide">•</span>   Japanese  <span class="dot_devide"></span>  Noodles  <span class="dot_devide">•</span>  Sashimi</p>
                        <div class="raing_box d-flex align-items-center">
                            <div class="raing_box_inner">
                                <span>4.9/5</span>
                                <i class="fas fa-star"></i>
                            </div>
                            <span  class="rating_text" >( 110 Rating )</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                <div class="product_wized mb_50">
                    <div class="thumb position-relative">
                        <a href="product_details.php" class="d-block">
                            <img src="img/product/product2.jpg" alt="">
                        </a>
                        <span class="product_tag position-absolute">Voucher: bijoy200 </span>
                    </div>
                    <div class="product_meta">
                        <h3><a href="product_details.php">Kosturi Kitchen Gulshan</a></h3>
                        <p> <span class="mark_text">$$$ </span>  Sushi <span class="dot_devide">•</span>   Japanese  <span class="dot_devide"></span>  Noodles  <span class="dot_devide">•</span>  Sashimi</p>
                        <div class="raing_box d-flex align-items-center">
                            <div class="raing_box_inner">
                                <span>4.9/5</span>
                                <i class="fas fa-star"></i>
                            </div>
                            <span  class="rating_text" >( 110 Rating )</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                <div class="product_wized mb_50">
                    <div class="thumb position-relative">
                        <a href="product_details.php" class="d-block">
                            <img src="img/product/product3.jpg" alt="">
                        </a>
                        <span class="product_tag position-absolute">15% OFF</span>
                    </div>
                    <div class="product_meta">
                        <h3><a href="product_details.php">Kosturi Kitchen Gulshan</a></h3>
                        <p> <span class="mark_text">$$$ </span>  Sushi <span class="dot_devide">•</span>   Japanese  <span class="dot_devide"></span>  Noodles  <span class="dot_devide">•</span>  Sashimi</p>
                        <div class="raing_box d-flex align-items-center">
                            <div class="raing_box_inner">
                                <span>4.9/5</span>
                                <i class="fas fa-star"></i>
                            </div>
                            <span  class="rating_text" >( 110 Rating )</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                <div class="product_wized mb_50">
                    <div class="thumb position-relative">
                        <a href="product_details.php" class="d-block">
                            <img src="img/product/product4.jpg" alt="">
                        </a>
                        <span class="product_tag position-absolute">Voucher: bijoy200 </span>
                    </div>
                    <div class="product_meta">
                        <h3><a href="product_details.php">Kosturi Kitchen Gulshan</a></h3>
                        <p> <span class="mark_text">$$$ </span>  Sushi <span class="dot_devide">•</span>   Japanese  <span class="dot_devide"></span>  Noodles  <span class="dot_devide">•</span>  Sashimi</p>
                        <div class="raing_box d-flex align-items-center">
                            <div class="raing_box_inner">
                                <span>4.9/5</span>
                                <i class="fas fa-star"></i>
                            </div>
                            <span  class="rating_text" >( 110 Rating )</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                <div class="product_wized mb_50">
                    <div class="thumb position-relative">
                        <a href="product_details.php" class="d-block">
                            <img src="img/product/product5.jpg" alt="">
                        </a>
                        <span class="product_tag position-absolute">15% OFF</span>
                    </div>
                    <div class="product_meta">
                        <h3><a href="product_details.php">Kosturi Kitchen Gulshan</a></h3>
                        <p> <span class="mark_text">$$$ </span>  Sushi <span class="dot_devide">•</span>   Japanese  <span class="dot_devide"></span>  Noodles  <span class="dot_devide">•</span>  Sashimi</p>
                        <div class="raing_box d-flex align-items-center">
                            <div class="raing_box_inner">
                                <span>4.9/5</span>
                                <i class="fas fa-star"></i>
                            </div>
                            <span  class="rating_text" >( 110 Rating )</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                <div class="product_wized mb_50">
                    <div class="thumb position-relative">
                        <a href="product_details.php" class="d-block">
                            <img src="img/product/product6.jpg" alt="">
                        </a>
                        <span class="product_tag position-absolute">Voucher: bijoy200 </span>
                    </div>
                    <div class="product_meta">
                        <h3><a href="product_details.php">Kosturi Kitchen Gulshan</a></h3>
                        <p> <span class="mark_text">$$$ </span>  Sushi <span class="dot_devide">•</span>   Japanese  <span class="dot_devide"></span>  Noodles  <span class="dot_devide">•</span>  Sashimi</p>
                        <div class="raing_box d-flex align-items-center">
                            <div class="raing_box_inner">
                                <span>4.9/5</span>
                                <i class="fas fa-star"></i>
                            </div>
                            <span  class="rating_text" >( 110 Rating )</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                <div class="product_wized mb_50">
                    <div class="thumb position-relative">
                        <a href="product_details.php" class="d-block">
                            <img src="img/product/product7.jpg" alt="">
                        </a>
                        <span class="product_tag position-absolute">15% OFF</span>
                    </div>
                    <div class="product_meta">
                        <h3><a href="product_details.php">Kosturi Kitchen Gulshan</a></h3>
                        <p> <span class="mark_text">$$$ </span>  Sushi <span class="dot_devide">•</span>   Japanese  <span class="dot_devide"></span>  Noodles  <span class="dot_devide">•</span>  Sashimi</p>
                        <div class="raing_box d-flex align-items-center">
                            <div class="raing_box_inner">
                                <span>4.9/5</span>
                                <i class="fas fa-star"></i>
                            </div>
                            <span  class="rating_text" >( 110 Rating )</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                <div class="product_wized mb_50">
                    <div class="thumb position-relative">
                        <a href="product_details.php" class="d-block">
                            <img src="img/product/product8.jpg" alt="">
                        </a>
                        <span class="product_tag position-absolute">15% OFF</span>
                    </div>
                    <div class="product_meta">
                        <h3><a href="product_details.php">Kosturi Kitchen Gulshan</a></h3>
                        <p> <span class="mark_text">$$$ </span>  Sushi <span class="dot_devide">•</span>   Japanese  <span class="dot_devide"></span>  Noodles  <span class="dot_devide">•</span>  Sashimi</p>
                        <div class="raing_box d-flex align-items-center">
                            <div class="raing_box_inner">
                                <span>4.9/5</span>
                                <i class="fas fa-star"></i>
                            </div>
                            <span  class="rating_text" >( 110 Rating )</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                <div class="product_wized mb_50">
                    <div class="thumb position-relative">
                        <a href="product_details.php" class="d-block">
                            <img src="img/product/product9.jpg" alt="">
                        </a>
                        <span class="product_tag position-absolute">15% OFF</span>
                    </div>
                    <div class="product_meta">
                        <h3><a href="product_details.php">Kosturi Kitchen Gulshan</a></h3>
                        <p> <span class="mark_text">$$$ </span>  Sushi <span class="dot_devide">•</span>   Japanese  <span class="dot_devide"></span>  Noodles  <span class="dot_devide">•</span>  Sashimi</p>
                        <div class="raing_box d-flex align-items-center">
                            <div class="raing_box_inner">
                                <span>4.9/5</span>
                                <i class="fas fa-star"></i>
                            </div>
                            <span  class="rating_text" >( 110 Rating )</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                <div class="product_wized mb_50">
                    <div class="thumb position-relative">
                        <a href="product_details.php" class="d-block">
                            <img src="img/product/product10.jpg" alt="">
                        </a>
                        <span class="product_tag position-absolute">15% OFF</span>
                    </div>
                    <div class="product_meta">
                        <h3><a href="product_details.php">Kosturi Kitchen Gulshan</a></h3>
                        <p> <span class="mark_text">$$$ </span>  Sushi <span class="dot_devide">•</span>   Japanese  <span class="dot_devide"></span>  Noodles  <span class="dot_devide">•</span>  Sashimi</p>
                        <div class="raing_box d-flex align-items-center">
                            <div class="raing_box_inner">
                                <span>4.9/5</span>
                                <i class="fas fa-star"></i>
                            </div>
                            <span  class="rating_text" >( 110 Rating )</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                <div class="product_wized mb_50">
                    <div class="thumb position-relative">
                        <a href="product_details.php" class="d-block">
                            <img src="img/product/product1.jpg" alt="">
                        </a>
                        <span class="product_tag position-absolute">15% OFF</span>
                    </div>
                    <div class="product_meta">
                        <h3><a href="product_details.php">Kosturi Kitchen Gulshan</a></h3>
                        <p> <span class="mark_text">$$$ </span>  Sushi <span class="dot_devide">•</span>   Japanese  <span class="dot_devide"></span>  Noodles  <span class="dot_devide">•</span>  Sashimi</p>
                        <div class="raing_box d-flex align-items-center">
                            <div class="raing_box_inner">
                                <span>4.9/5</span>
                                <i class="fas fa-star"></i>
                            </div>
                            <span  class="rating_text" >( 110 Rating )</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                <div class="product_wized mb_50">
                    <div class="thumb position-relative">
                        <a href="product_details.php" class="d-block">
                            <img src="img/product/product1.jpg" alt="">
                        </a>
                        <span class="product_tag position-absolute">15% OFF</span>
                    </div>
                    <div class="product_meta">
                        <h3><a href="product_details.php">Kosturi Kitchen Gulshan</a></h3>
                        <p> <span class="mark_text">$$$ </span>  Sushi <span class="dot_devide">•</span>   Japanese  <span class="dot_devide"></span>  Noodles  <span class="dot_devide">•</span>  Sashimi</p>
                        <div class="raing_box d-flex align-items-center">
                            <div class="raing_box_inner">
                                <span>4.9/5</span>
                                <i class="fas fa-star"></i>
                            </div>
                            <span  class="rating_text" >( 110 Rating )</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                <div class="product_wized mb_50">
                    <div class="thumb position-relative">
                        <a href="product_details.php" class="d-block">
                            <img src="img/product/product1.jpg" alt="">
                        </a>
                        <span class="product_tag position-absolute">15% OFF</span>
                    </div>
                    <div class="product_meta">
                        <h3><a href="product_details.php">Kosturi Kitchen Gulshan</a></h3>
                        <p> <span class="mark_text">$$$ </span>  Sushi <span class="dot_devide">•</span>   Japanese  <span class="dot_devide"></span>  Noodles  <span class="dot_devide">•</span>  Sashimi</p>
                        <div class="raing_box d-flex align-items-center">
                            <div class="raing_box_inner">
                                <span>4.9/5</span>
                                <i class="fas fa-star"></i>
                            </div>
                            <span  class="rating_text" >( 110 Rating )</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                <div class="product_wized mb_50">
                    <div class="thumb position-relative">
                        <a href="product_details.php" class="d-block">
                            <img src="img/product/product1.jpg" alt="">
                        </a>
                        <span class="product_tag position-absolute">15% OFF</span>
                    </div>
                    <div class="product_meta">
                        <h3><a href="product_details.php">Kosturi Kitchen Gulshan</a></h3>
                        <p> <span class="mark_text">$$$ </span>  Sushi <span class="dot_devide">•</span>   Japanese  <span class="dot_devide"></span>  Noodles  <span class="dot_devide">•</span>  Sashimi</p>
                        <div class="raing_box d-flex align-items-center">
                            <div class="raing_box_inner">
                                <span>4.9/5</span>
                                <i class="fas fa-star"></i>
                            </div>
                            <span  class="rating_text" >( 110 Rating )</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                <div class="product_wized mb_50">
                    <div class="thumb position-relative">
                        <a href="product_details.php" class="d-block">
                            <img src="img/product/product1.jpg" alt="">
                        </a>
                        <span class="product_tag position-absolute">15% OFF</span>
                    </div>
                    <div class="product_meta">
                        <h3><a href="product_details.php">Kosturi Kitchen Gulshan</a></h3>
                        <p> <span class="mark_text">$$$ </span>  Sushi <span class="dot_devide">•</span>   Japanese  <span class="dot_devide"></span>  Noodles  <span class="dot_devide">•</span>  Sashimi</p>
                        <div class="raing_box d-flex align-items-center">
                            <div class="raing_box_inner">
                                <span>4.9/5</span>
                                <i class="fas fa-star"></i>
                            </div>
                            <span  class="rating_text" >( 110 Rating )</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                <div class="product_wized mb_50">
                    <div class="thumb position-relative">
                        <a href="product_details.php" class="d-block">
                            <img src="img/product/product1.jpg" alt="">
                        </a>
                        <span class="product_tag position-absolute">15% OFF</span>
                    </div>
                    <div class="product_meta">
                        <h3><a href="product_details.php">Kosturi Kitchen Gulshan</a></h3>
                        <p> <span class="mark_text">$$$ </span>  Sushi <span class="dot_devide">•</span>   Japanese  <span class="dot_devide"></span>  Noodles  <span class="dot_devide">•</span>  Sashimi</p>
                        <div class="raing_box d-flex align-items-center">
                            <div class="raing_box_inner">
                                <span>4.9/5</span>
                                <i class="fas fa-star"></i>
                            </div>
                            <span  class="rating_text" >( 110 Rating )</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 text-center">
                <a href="#" class="theme_line_btn shadow_btn max_btn">Load more Restaurent</a>
            </div>
        </div>
    </div>
</div>
<!-- infix_popular_area::end  -->

<!-- infix_subscribe_area::start  -->
<div class="infix_subscribe_area">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-9">
                <div class="infix_subscribe_box">
                    <div class="infix_subscribe_text">
                        <h3>Get started for free!</h3>
                        <p>Order lunch, fuel for meetings or late-night deliveries 
                            your favorite restaurants desk near you.</p>
                    </div>
                    <div class="infix_subscribe_form">
                        <input class="infix_primary_input" type="text" placeholder="Type e-mail address">
                        <button class="black_btn shadow_btn width_160">Get started</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- infix_subscribe_area::end  -->


<?php include 'include/footer_content2.php' ?>
<?php include 'include/footer.php' ?>