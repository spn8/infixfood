<?php include 'include/header.php' ?>
<?php include 'include/menu.php' ?>


<!-- account_info_area::start  -->
<div class="account_info_area">
    <div class="container">
        <div class="row">
            <div class="col-xl-7 offset-xl-2">
                <div class="account_profile_wrapper">
                    <div class="account_profile_thumb text-center mb_30">
                        <div class="thumb">
                            <img src="img/author.png" alt="">
                        </div>
                        <h4>Robert Downey JR.</h4>
                        <p>UX/UI Designer</p>
                    </div>
                    <div class="account_profile_form">
                        <div class="account_title">
                            <h3 class="fs-4 f_w_700 ">Account Details</h3>
                        </div>
                        <form action="#">
                            <div class="row">
                                <div class="col-lg-12">
                                    <label class="primary_label mb_20_imporatnt">Email Address</label>
                                    <input name="email" placeholder="Type e-mail address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Type e-mail address'" class="primary_input3 mb_20" required="" type="email">
                                </div>
                                <div class="col-lg-12  ">
                                    <label class="primary_label mb_20_imporatnt">First Name</label>
                                    <input name="First" placeholder="Enter first name" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter first name'" class="primary_input3 mb_20" required="" type="text">
                                </div>
                                <div class="col-lg-12 ">
                                    <label class="primary_label mb_20_imporatnt">Last Name</label>
                                    <input name="Last" placeholder="Enter last name" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter last name'" class="primary_input3 mb_20" required="" type="text">
                                </div>
                                <div class="col-lg-12 ">
                                    <label class="primary_label mb_20_imporatnt">Mobile Number</label>
                                    <input name="Mobile" placeholder="+880 XXX XXXX XXX" onfocus="this.placeholder = ''" onblur="this.placeholder = '+880 XXX XXXX XXX'" class="primary_input3 mb_55" required="" type="text">
                                </div>
                                <div class="col-12">
                                    <div class="account_title">
                                        <h3 class="fs-4 f_w_700">Change Password</h3>
                                    </div>
                                </div>
                                <div class="col-lg-12 ">
                                    <label class="primary_label mb_20_imporatnt">Current Password</label>
                                    <input name="Mobile" placeholder="Enter current password" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter current password'" class="primary_input3 mb_20" required="" type="text">
                                </div>
                                <div class="col-lg-12 ">
                                    <label class="primary_label mb_20_imporatnt">New Password</label>
                                    <input name="Mobile" placeholder="Enter new password" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter new password'" class="primary_input3 mb_20" required="" type="text">
                                </div>
                                <div class="col-lg-12 ">
                                    <label class="primary_label mb_20_imporatnt">Re-enter New Password</label>
                                    <input name="Mobile" placeholder="Enter new password" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter new password'" class="primary_input3" required="" type="text">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- account_info_area::end  -->


<!-- infix_subscribe_area::start  -->
<div class="infix_subscribe_area">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-9">
                <div class="infix_subscribe_box">
                    <div class="infix_subscribe_text">
                        <h3>Get started for free!</h3>
                        <p>Order lunch, fuel for meetings or late-night deliveries 
                            your favorite restaurants desk near you.</p>
                    </div>
                    <div class="infix_subscribe_form">
                        <input class="infix_primary_input" type="text" placeholder="Type e-mail address">
                        <button class="black_btn shadow_btn width_160">Get started</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- infix_subscribe_area::end  -->


<?php include 'include/footer_content2.php' ?>
<?php include 'include/footer.php' ?>