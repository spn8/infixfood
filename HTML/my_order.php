<?php include 'include/header.php' ?>
<?php include 'include/menu.php' ?>

<!-- my_order_area::start  -->
<div class="my_order_area section_spacing5">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xxl-4 col-xl-5 col-lg-6 col-md-8">
                <div class="myOrder_wrapper">
                    <h3 class="font_30 f_w_700 mb_23">Active Order</h3>
                </div>
                <div class="order_wrapper mb_45">
                    <div class="single_order d-flex flex-wrap gap_10 ">
                        <div class="order_content flex-fill">
                            <h4><a href="order_details.php">Takeout Restaurant - Manhattan</a></h4>
                            <p class="order_text">Delivery By: <span>InfixFood</span></p>
                            <p class="order_date">26 December, 2020</p>
                            <p class="order_text2"><span>2</span>x Chicken Satay</p>
                        </div>
                        <div class="order_info d-flex flex-column gap_r15 text-end">
                            <span class="prise_text">+ USD 324.35</span>
                        </div>
                    </div>
                    <div class="single_order d-flex flex-wrap gap_10 ">
                        <div class="order_content flex-fill">
                            <h4><a href="order_details.php">Burger King Restaurant</a></h4>
                            <p class="order_text">Delivery By: <span>InfixFood</span></p>
                            <p class="order_date">26 December, 2020</p>
                            <p class="order_text2"><span>2</span>x Chicken Satay</p>
                        </div>
                        <div class="order_info d-flex flex-column gap_r15 text-end">
                            <span class="prise_text">+ USD 324.35</span>
                        </div>
                    </div>
                </div>
                <div class="myOrder_wrapper">
                    <h3 class="font_30 f_w_700 mb_23">Previous Order</h3>
                </div>
                <div class="order_wrapper">
                    <div class="single_order d-flex flex-wrap gap_10 ">
                        <div class="order_content flex-fill">
                            <h4><a href="order_details.php">Dominos Pizza</a></h4>
                            <p class="order_text">Delivery By: <span>InfixFood</span></p>
                            <p class="order_date">26 December, 2020</p>
                            <p class="order_text2"><span>2</span>x Chicken Satay</p>
                        </div>
                        <div class="order_info d-flex flex-column gap_r15 text-end">
                            <span class="prise_text">+ USD 324.35</span>
                            <button class="theme_line_btn style3 text-capitalize fw-bold" >Reorder</button>
                        </div>
                    </div>
                    <div class="single_order d-flex flex-wrap gap_10 ">
                        <div class="order_content flex-fill">
                            <h4><a href="order_details.php">Dominos Pizza</a></h4>
                            <p class="order_text">Delivery By: <span>InfixFood</span></p>
                            <p class="order_date">26 December, 2020</p>
                            <p class="order_text2"><span>2</span>x Chicken Satay</p>
                        </div>
                        <div class="order_info d-flex flex-column gap_r15 text-end">
                            <span class="prise_text">+ USD 324.35</span>
                            <button class="theme_line_btn style3 text-capitalize fw-bold" >Reorder</button>
                        </div>
                    </div>
                    <div class="single_order d-flex flex-wrap gap_10 ">
                        <div class="order_content flex-fill">
                            <h4><a href="order_details.php">Dominos Pizza</a></h4>
                            <p class="order_text">Delivery By: <span>InfixFood</span></p>
                            <p class="order_date">26 December, 2020</p>
                            <p class="order_text2"><span>2</span>x Chicken Satay</p>
                        </div>
                        <div class="order_info d-flex flex-column gap_r15 text-end">
                            <span class="prise_text">+ USD 324.35</span>
                            <button class="theme_line_btn style3 text-capitalize fw-bold" >Reorder</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- my_order_area::end  -->

<!-- infix_subscribe_area::start  -->
<div class="infix_subscribe_area">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-9">
                <div class="infix_subscribe_box">
                    <div class="infix_subscribe_text">
                        <h3>Get started for free!</h3>
                        <p>Order lunch, fuel for meetings or late-night deliveries 
                            your favorite restaurants desk near you.</p>
                    </div>
                    <div class="infix_subscribe_form">
                        <input class="infix_primary_input" type="text" placeholder="Type e-mail address">
                        <button class="black_btn shadow_btn width_160">Get started</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- infix_subscribe_area::end  -->
<?php include 'include/footer_content2.php' ?>
<?php include 'include/footer.php' ?>