<?php include 'include/header.php' ?>
<?php include 'include/menu.php' ?>

<!-- banner::start  -->
<div class="breadcrumb_area style2">
    <div class="breadcrumb_iner bradcam_bg_2"></div>
</div>
<!--/ banner::end  -->

<!-- product_details_area::start  -->
<div class="product_details_area">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xxl-8 col-xl-9 col-lg-10">
                <div class="product_details_head d-flex gap-3 align-items-start flex-wrap">
                    <div class="product_meta flex-fill">
                        <h3 class="d-flex align-items-center  gap_10 flex-wrap "><a data-bs-toggle="modal" data-bs-target="#about_modal" href="product_details.php">Kosturi Kitchen Gulshan</a> <span class="product_tag">15% OFF</span> </h3>
                        <p> <span class="mark_text">$$$ </span>  Sushi <span class="dot_devide">•</span>   Japanese  <span class="dot_devide"></span>  Noodles  <span class="dot_devide">•</span>  Sashimi</p>
                        <div class="raing_box d-flex align-items-center">
                            <div class="raing_box_inner">
                                <span>4.9/5</span>
                                <i class="fas fa-star"></i>
                            </div>
                            <span class="rating_text">( 110 Rating )</span>
                        </div>
                    </div>
                    <div data-bs-toggle="modal" data-bs-target="#invite_modal" class="invite_people d-inline-flex align-items-center">
                        <img src="img/svgs/invitation.svg" alt="">
                        <span>Invite People</span>
                        <i class="far fa-question-circle"></i>
                    </div>
                </div>
                <ul class="nav infix_food_tabs" id="myTab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <button class="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#home" type="button" role="tab" aria-controls="home" aria-selected="true">Single Dish</button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="nav-link" id="profile-tab" data-bs-toggle="tab" data-bs-target="#profile" type="button" role="tab" aria-controls="profile" aria-selected="false">Special Dish</button>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <!-- content  -->
                        <div class="product_details_body">
                            <div class="product_details_single">
                                <h4 class="fs-4 f_w_700 mb_30">Single Dish</h4>
                                <div class="product_details_wrapper  ">
                                    <div class="product_d_wized position-relative">
                                        <h4 class="m-0 d-flex align-items-center justify-content-between gap-1 flex-wrap"><a  href="#">Kosturi Kitchen Gulshan</a> <span class="prise ">USD 324.35</span></h4>
                                        <p> Sushi <span class="dot_devide">•</span>   Japanese  <span class="dot_devide"></span>  Noodles  <span class="dot_devide">•</span>  Sashimi</p>
                                        <span class="addcart_action" data-bs-target="#cart_modal" data-bs-toggle="modal">
                                            <i class="ti-plus"></i>
                                            <span>Add to Cart</span>
                                        </span>
                                    </div>
                                    <div class="product_d_wized position-relative">
                                        <h4 class="m-0 d-flex align-items-center justify-content-between gap-1 flex-wrap"><a  href="#">Kosturi Kitchen Gulshan</a> <span class="prise ">USD 324.35</span></h4>
                                        <p> Sushi <span class="dot_devide">•</span>   Japanese  <span class="dot_devide"></span>  Noodles  <span class="dot_devide">•</span>  Sashimi</p>
                                        <span class="addcart_action" data-bs-target="#cart_modal" data-bs-toggle="modal">
                                            <i class="ti-plus"></i>
                                            <span>Add to Cart</span>
                                        </span>
                                    </div>
                                    <div class="product_d_wized position-relative">
                                        <h4 class="m-0 d-flex align-items-center justify-content-between gap-1 flex-wrap"><a  href="#">Kosturi Kitchen Gulshan</a> <span class="prise ">USD 324.35</span></h4>
                                        <p> Sushi <span class="dot_devide">•</span>   Japanese  <span class="dot_devide"></span>  Noodles  <span class="dot_devide">•</span>  Sashimi</p>
                                        <span class="addcart_action" data-bs-target="#cart_modal" data-bs-toggle="modal">
                                            <i class="ti-plus"></i>
                                            <span>Add to Cart</span>
                                        </span>
                                    </div>
                                    <div class="product_d_wized position-relative">
                                        <h4 class="m-0 d-flex align-items-center justify-content-between gap-1 flex-wrap"><a  href="#">Kosturi Kitchen Gulshan</a> <span class="prise ">USD 324.35</span></h4>
                                        <p> Sushi <span class="dot_devide">•</span>   Japanese  <span class="dot_devide"></span>  Noodles  <span class="dot_devide">•</span>  Sashimi</p>
                                        <span class="addcart_action" data-bs-target="#cart_modal" data-bs-toggle="modal">
                                            <i class="ti-plus"></i>
                                            <span>Add to Cart</span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="product_details_single">
                                <h4 class="fs-4 f_w_700 mb_30">Single Dish</h4>
                                <div class="product_details_wrapper  ">
                                    <div class="product_d_wized position-relative">
                                        <h4 class="m-0 d-flex align-items-center justify-content-between gap-1 flex-wrap"><a  href="#">Kosturi Kitchen Gulshan</a> <span class="prise ">USD 324.35</span></h4>
                                        <p> Sushi <span class="dot_devide">•</span>   Japanese  <span class="dot_devide"></span>  Noodles  <span class="dot_devide">•</span>  Sashimi</p>
                                        <span class="addcart_action" data-bs-target="#cart_modal" data-bs-toggle="modal">
                                            <i class="ti-plus"></i>
                                            <span>Add to Cart</span>
                                        </span>
                                    </div>
                                    <div class="product_d_wized position-relative">
                                        <h4 class="m-0 d-flex align-items-center justify-content-between gap-1 flex-wrap"><a  href="#">Kosturi Kitchen Gulshan</a> <span class="prise ">USD 324.35</span></h4>
                                        <p> Sushi <span class="dot_devide">•</span>   Japanese  <span class="dot_devide"></span>  Noodles  <span class="dot_devide">•</span>  Sashimi</p>
                                        <span class="addcart_action" data-bs-target="#cart_modal" data-bs-toggle="modal">
                                            <i class="ti-plus"></i>
                                            <span>Add to Cart</span>
                                        </span>
                                    </div>
                                    <div class="product_d_wized position-relative">
                                        <h4 class="m-0 d-flex align-items-center justify-content-between gap-1 flex-wrap"><a  href="#">Kosturi Kitchen Gulshan</a> <span class="prise ">USD 324.35</span></h4>
                                        <p> Sushi <span class="dot_devide">•</span>   Japanese  <span class="dot_devide"></span>  Noodles  <span class="dot_devide">•</span>  Sashimi</p>
                                        <span class="addcart_action" data-bs-target="#cart_modal" data-bs-toggle="modal">
                                            <i class="ti-plus"></i>
                                            <span>Add to Cart</span>
                                        </span>
                                    </div>
                                    <div class="product_d_wized position-relative">
                                        <h4 class="m-0 d-flex align-items-center justify-content-between gap-1 flex-wrap"><a  href="#">Kosturi Kitchen Gulshan</a> <span class="prise ">USD 324.35</span></h4>
                                        <p> Sushi <span class="dot_devide">•</span>   Japanese  <span class="dot_devide"></span>  Noodles  <span class="dot_devide">•</span>  Sashimi</p>
                                        <span class="addcart_action" data-bs-target="#cart_modal" data-bs-toggle="modal">
                                            <i class="ti-plus"></i>
                                            <span>Add to Cart</span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- content ::end -->
                    </div>
                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <!-- content  -->
                        <div class="product_details_body">
                            <div class="product_details_single">
                                <h4 class="fs-4 f_w_700 mb_30">Special Dish</h4>
                                <div class="product_details_wrapper  ">
                                    <div class="product_d_wized position-relative">
                                        <h4 class="m-0 d-flex align-items-center justify-content-between gap-1 flex-wrap"><a class="" href="#">Kosturi Kitchen Gulshan</a> <span class="prise ">USD 324.35</span></h4>
                                        <p> Sushi <span class="dot_devide">•</span>   Japanese  <span class="dot_devide"></span>  Noodles  <span class="dot_devide">•</span>  Sashimi</p>
                                        <span class="addcart_action">
                                            <i class="ti-plus"></i>
                                            <span>Add to Cart</span>
                                        </span>
                                    </div>
                                    <div class="product_d_wized position-relative">
                                        <h4 class="m-0 d-flex align-items-center justify-content-between gap-1 flex-wrap"><a class="" href="#">Kosturi Kitchen Gulshan</a> <span class="prise ">USD 324.35</span></h4>
                                        <p> Sushi <span class="dot_devide">•</span>   Japanese  <span class="dot_devide"></span>  Noodles  <span class="dot_devide">•</span>  Sashimi</p>
                                        <span class="addcart_action">
                                            <i class="ti-plus"></i>
                                            <span>Add to Cart</span>
                                        </span>
                                    </div>
                                    <div class="product_d_wized position-relative">
                                        <h4 class="m-0 d-flex align-items-center justify-content-between gap-1 flex-wrap"><a class="" href="#">Kosturi Kitchen Gulshan</a> <span class="prise ">USD 324.35</span></h4>
                                        <p> Sushi <span class="dot_devide">•</span>   Japanese  <span class="dot_devide"></span>  Noodles  <span class="dot_devide">•</span>  Sashimi</p>
                                        <span class="addcart_action">
                                            <i class="ti-plus"></i>
                                            <span>Add to Cart</span>
                                        </span>
                                    </div>
                                    <div class="product_d_wized position-relative">
                                        <h4 class="m-0 d-flex align-items-center justify-content-between gap-1 flex-wrap"><a class="" href="#">Kosturi Kitchen Gulshan</a> <span class="prise ">USD 324.35</span></h4>
                                        <p> Sushi <span class="dot_devide">•</span>   Japanese  <span class="dot_devide"></span>  Noodles  <span class="dot_devide">•</span>  Sashimi</p>
                                        <span class="addcart_action">
                                            <i class="ti-plus"></i>
                                            <span>Add to Cart</span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="product_details_single">
                                <h4 class="fs-4 f_w_700 mb_30">Single Dish</h4>
                                <div class="product_details_wrapper  ">
                                    <div class="product_d_wized position-relative">
                                        <h4 class="m-0 d-flex align-items-center justify-content-between gap-1 flex-wrap"><a class="" href="#">Kosturi Kitchen Gulshan</a> <span class="prise ">USD 324.35</span></h4>
                                        <p> Sushi <span class="dot_devide">•</span>   Japanese  <span class="dot_devide"></span>  Noodles  <span class="dot_devide">•</span>  Sashimi</p>
                                        <span class="addcart_action">
                                            <i class="ti-plus"></i>
                                            <span>Add to Cart</span>
                                        </span>
                                    </div>
                                    <div class="product_d_wized position-relative">
                                        <h4 class="m-0 d-flex align-items-center justify-content-between gap-1 flex-wrap"><a class="" href="#">Kosturi Kitchen Gulshan</a> <span class="prise ">USD 324.35</span></h4>
                                        <p> Sushi <span class="dot_devide">•</span>   Japanese  <span class="dot_devide"></span>  Noodles  <span class="dot_devide">•</span>  Sashimi</p>
                                        <span class="addcart_action">
                                            <i class="ti-plus"></i>
                                            <span>Add to Cart</span>
                                        </span>
                                    </div>
                                    <div class="product_d_wized position-relative">
                                        <h4 class="m-0 d-flex align-items-center justify-content-between gap-1 flex-wrap"><a class="" href="#">Kosturi Kitchen Gulshan</a> <span class="prise ">USD 324.35</span></h4>
                                        <p> Sushi <span class="dot_devide">•</span>   Japanese  <span class="dot_devide"></span>  Noodles  <span class="dot_devide">•</span>  Sashimi</p>
                                        <span class="addcart_action">
                                            <i class="ti-plus"></i>
                                            <span>Add to Cart</span>
                                        </span>
                                    </div>
                                    <div class="product_d_wized position-relative">
                                        <h4 class="m-0 d-flex align-items-center justify-content-between gap-1 flex-wrap"><a class="" href="#">Kosturi Kitchen Gulshan</a> <span class="prise ">USD 324.35</span></h4>
                                        <p> Sushi <span class="dot_devide">•</span>   Japanese  <span class="dot_devide"></span>  Noodles  <span class="dot_devide">•</span>  Sashimi</p>
                                        <span class="addcart_action">
                                            <i class="ti-plus"></i>
                                            <span>Add to Cart</span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- content ::end -->
                    </div>
                </div>

                
            </div>
        </div>
    </div>
</div>
<!-- product_details_area::start  -->

<!-- infix_subscribe_area::start  -->
<div class="infix_subscribe_area">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-9">
                <div class="infix_subscribe_box">
                    <div class="infix_subscribe_text">
                        <h3>Get started for free!</h3>
                        <p>Order lunch, fuel for meetings or late-night deliveries 
                            your favorite restaurants desk near you.</p>
                    </div>
                    <div class="infix_subscribe_form">
                        <input class="infix_primary_input" type="text" placeholder="Type e-mail address">
                        <button class="black_btn shadow_btn width_160">Get started</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- infix_subscribe_area::end  -->


<?php include 'include/footer_content2.php' ?>
<?php include 'include/footer.php' ?>