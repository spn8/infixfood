<?php include 'include/header.php' ?>
<?php include 'include/menu2.php' ?>

<!-- banner::start  -->
<div class="breadcrumb_area">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="breadcrumb_iner bradcam_bg_1" >
                    <div class="bradcam_text">
                        <span class="f_w_500 text-uppercase">#Bijoy100</span>
                        <h3>Use This Code and Get $100 Less 
                        on Selected Restaurant.
                        </h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--/ banner::end  -->
<!-- infix_search_area::start  -->
<div class="infix_search_area section_spacing3">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-6 col-lg-8 d-flex align-items-center justify-content-center">
                <div class="searchFilter_wrap position-relative w-100">
                    <div class="input-group infix_searchFilter">
                        <input type="text" class="form-control shadow-none recent_serach_toggler" placeholder="2593 Timbercrest Road, Chisana, USA" aria-label="2593 Timbercrest Road, Chisana, USA" aria-describedby="button-addon2">
                        <button class="btn shadow-none filter_srs_toggle" type="button" id="button-addon2">
                            <img src="img/svgs/filter_icon.svg" alt="">
                            <span>Filter</span>
                            <i class="ti-angle-down"></i>
                        </button>
                    </div>
                    <!-- recent_search_wrap::start  -->
                    <div class="recent_search_wrap position-absolute ">
                        <h4 class="fs-6 f_w_700 text-uppercase mb_15">Recent searches</h4>
                        <ul class="recent_lists mb_30">
                            <li>
                                <a href="#"> <img src="img/svgs/clock.svg" alt=""> <span>Sultan’s Dine - Dhanmondi</span></a>
                                <i class="ti-close hide_recent"></i>
                            </li>
                            <li>
                                <a href="#"> <img src="img/svgs/clock.svg" alt=""> <span>Sultan’s Dine - Dhanmondi</span></a>
                                <i class="ti-close hide_recent"></i>
                            </li>
                            <li>
                                <a href="#"> <img src="img/svgs/clock.svg" alt=""> <span>Sultan’s Dine - Dhanmondi</span></a>
                                <i class="ti-close hide_recent"></i>
                            </li>
                        </ul>
                        <h4 class="fs-6 f_w_700 text-uppercase mb_25">Popular searches</h4>
                        <div class="popular_search">
                            <a href="#">Pizza</a>
                            <a href="#">Pizza</a>
                            <a href="#">Pizza</a>
                            <a href="#">Pizza</a>
                            <a href="#">Pizza</a>
                            <a href="#">Pizza</a>
                            <a href="#">Pizza</a>
                            <a href="#">Pizza</a>
                            <a href="#">Pizza</a>
                            <a href="#">Pizza</a>
                            <a href="#">Pizza</a>
                            <a href="#">Pizza</a>
                        </div>
                    </div>
                    <!-- recent_search_wrap::start  -->
                    <div class="search_filter_wrapper">
                        <div class="search_filter_head d-flex align-items-center">
                            <div class="icon d-flex align-items-center gap_10 ">
                                <img src="img/svgs/filter_icon.svg" alt="">
                                <span class=" f_s_14 f_w_500 text-uppercase ">Filter</span>
                            </div>
                            <div class="earch_filter_text flex-fill overflow-hidden">
                                <h4 class="mb-0 text-nowrap">$$, Free delivery, American, BBQ</h4>
                            </div>
                            <div class="search_clear_toggle d-flex align-items-center">
                                <span class="text-nowrap">Clear all</span>
                                <i class="ti-angle-up close_filter"></i>
                            </div>
                        </div>
                        <div class="search_filter_body">
                            <h4 class="fs-6 text-uppercase f_w_700 mb_18 ">PRICE</h4>
                            <div class="search_filter_prise">
                                <button class="filter_prise" >$</button>
                                <button class="filter_prise active" >$$</button>
                                <button class="filter_prise" >$$$</button>
                            </div>
                            <div class="filter_group_box">
                                <h4 class="fs-6 text-uppercase f_w_700 mb_15 ">OFFERS</h4>
                                <div class="filter_group">
                                    <label class="primary_checkbox d-flex">
                                        <input type="checkbox">
                                        <span class="checkmark mr_15"></span>
                                        <span class="label_name">Free Delivery</span>
                                    </label>
                                    <label class="primary_checkbox d-flex">
                                        <input type="checkbox">
                                        <span class="checkmark mr_15"></span>
                                        <span class="label_name">Online Payment</span>
                                    </label>
                                    <label class="primary_checkbox d-flex">
                                        <input type="checkbox">
                                        <span class="checkmark mr_15"></span>
                                        <span class="label_name">Offer</span>
                                    </label>
                                </div>
                            </div>
                            <div class="filter_group_box">
                                <h4 class="fs-6 text-uppercase f_w_700 mb_15 ">CUISINES</h4>
                                <div class="filter_group">
                                    <label class="primary_checkbox d-flex">
                                        <input type="checkbox">
                                        <span class="checkmark mr_15"></span>
                                        <span class="label_name">Free Delivery</span>
                                    </label>
                                    <label class="primary_checkbox d-flex">
                                        <input type="checkbox">
                                        <span class="checkmark mr_15"></span>
                                        <span class="label_name">Online Payment</span>
                                    </label>
                                    <label class="primary_checkbox d-flex">
                                        <input type="checkbox">
                                        <span class="checkmark mr_15"></span>
                                        <span class="label_name">Offer</span>
                                    </label>
                                    <label class="primary_checkbox d-flex">
                                        <input type="checkbox">
                                        <span class="checkmark mr_15"></span>
                                        <span class="label_name">Free Delivery</span>
                                    </label>
                                    <label class="primary_checkbox d-flex">
                                        <input type="checkbox">
                                        <span class="checkmark mr_15"></span>
                                        <span class="label_name">Online Payment</span>
                                    </label>
                                    <label class="primary_checkbox d-flex">
                                        <input type="checkbox">
                                        <span class="checkmark mr_15"></span>
                                        <span class="label_name">Offer</span>
                                    </label>
                                    <label class="primary_checkbox d-flex">
                                        <input type="checkbox">
                                        <span class="checkmark mr_15"></span>
                                        <span class="label_name">Free Delivery</span>
                                    </label>
                                    <label class="primary_checkbox d-flex">
                                        <input type="checkbox">
                                        <span class="checkmark mr_15"></span>
                                        <span class="label_name">Online Payment</span>
                                    </label>
                                    <label class="primary_checkbox d-flex">
                                        <input type="checkbox">
                                        <span class="checkmark mr_15"></span>
                                        <span class="label_name">Offer</span>
                                    </label>
                                    <label class="primary_checkbox d-flex">
                                        <input type="checkbox">
                                        <span class="checkmark mr_15"></span>
                                        <span class="label_name">Free Delivery</span>
                                    </label>
                                    <label class="primary_checkbox d-flex">
                                        <input type="checkbox">
                                        <span class="checkmark mr_15"></span>
                                        <span class="label_name">Online Payment</span>
                                    </label>
                                    <label class="primary_checkbox d-flex">
                                        <input type="checkbox">
                                        <span class="checkmark mr_15"></span>
                                        <span class="label_name">Offer</span>
                                    </label>
                                    <label class="primary_checkbox d-flex">
                                        <input type="checkbox">
                                        <span class="checkmark mr_15"></span>
                                        <span class="label_name">Free Delivery</span>
                                    </label>
                                    <label class="primary_checkbox d-flex">
                                        <input type="checkbox">
                                        <span class="checkmark mr_15"></span>
                                        <span class="label_name">Online Payment</span>
                                    </label>
                                    <label class="primary_checkbox d-flex">
                                        <input type="checkbox">
                                        <span class="checkmark mr_15"></span>
                                        <span class="label_name">Offer</span>
                                    </label>
                                </div>
                            </div>
                            <div class="filter_group_box">
                                <h4 class="fs-6 text-uppercase f_w_700 mb_15 ">Voucher</h4>
                                <div class="filter_group">
                                    <label class="primary_checkbox d-flex">
                                        <input type="checkbox">
                                        <span class="checkmark mr_15"></span>
                                        <span class="label_name">Upto 50% off</span>
                                    </label>
                                    <label class="primary_checkbox d-flex">
                                        <input type="checkbox">
                                        <span class="checkmark mr_15"></span>
                                        <span class="label_name">KHANA100</span>
                                    </label>
                                    <label class="primary_checkbox d-flex">
                                        <input type="checkbox">
                                        <span class="checkmark mr_15"></span>
                                        <span class="label_name">Bijoy100</span>
                                    </label>
                                </div>
                            </div>
                            <div class="filter_group_box">
                                <h4 class="fs-6 text-uppercase f_w_700 mb_15 ">OTHER VARIETIES</h4>
                                <div class="filter_group">
                                    <label class="primary_checkbox d-flex">
                                        <input type="checkbox">
                                        <span class="checkmark mr_15"></span>
                                        <span class="label_name">Free Delivery</span>
                                    </label>
                                    <label class="primary_checkbox d-flex">
                                        <input type="checkbox">
                                        <span class="checkmark mr_15"></span>
                                        <span class="label_name">Online Payment</span>
                                    </label>
                                    <label class="primary_checkbox d-flex">
                                        <input type="checkbox">
                                        <span class="checkmark mr_15"></span>
                                        <span class="label_name">Offer</span>
                                    </label>
                                    <label class="primary_checkbox d-flex">
                                        <input type="checkbox">
                                        <span class="checkmark mr_15"></span>
                                        <span class="label_name">Free Delivery</span>
                                    </label>
                                    <label class="primary_checkbox d-flex">
                                        <input type="checkbox">
                                        <span class="checkmark mr_15"></span>
                                        <span class="label_name">Online Payment</span>
                                    </label>
                                    <label class="primary_checkbox d-flex">
                                        <input type="checkbox">
                                        <span class="checkmark mr_15"></span>
                                        <span class="label_name">Offer</span>
                                    </label>
                                    <label class="primary_checkbox d-flex">
                                        <input type="checkbox">
                                        <span class="checkmark mr_15"></span>
                                        <span class="label_name">Free Delivery</span>
                                    </label>
                                    <label class="primary_checkbox d-flex">
                                        <input type="checkbox">
                                        <span class="checkmark mr_15"></span>
                                        <span class="label_name">Online Payment</span>
                                    </label>
                                    <label class="primary_checkbox d-flex">
                                        <input type="checkbox">
                                        <span class="checkmark mr_15"></span>
                                        <span class="label_name">Offer</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- infix_search_area::end  -->

<!-- infix_popular_area::start  -->
<div class="infix_section section_spacing2 pt-0">
    <div class="container">
        <div class="row mb_25">
            <div class="col-12">
                <div class="section__title mb_27">
                    <h3 class="heading fs-4 ">Popular Restaurants</h3>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                <div class="product_wized mb_30">
                    <div class="thumb position-relative">
                        <a href="product_details.php" class="d-block">
                            <img src="img/product/product1.jpg" alt="">
                        </a>
                        <span class="product_tag position-absolute">15% OFF</span>
                        <span class="product_tag style2 position-absolute">Voucher: KFC 150</span>
                        <span class="date_tag position-absolute">
                            <h6>25</h6>
                            <p>MIN</p>
                        </span>
                    </div>
                    <div class="product_meta">
                        <h3><a href="product_details.php">Kosturi Kitchen Gulshan</a></h3>
                        <p> <span class="mark_text">$$$ </span>  Sushi <span class="dot_devide">•</span>   Japanese  <span class="dot_devide"></span>  Noodles  <span class="dot_devide">•</span>  Sashimi</p>
                        <div class="raing_box d-flex align-items-center">
                            <div class="raing_box_inner">
                                <span>4.9/5</span>
                                <i class="fas fa-star"></i>
                            </div>
                            <span  class="rating_text" >( 110 Rating )</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                <div class="product_wized mb_30">
                    <div class="thumb position-relative">
                        <a href="product_details.php" class="d-block">
                            <img src="img/product/product1.jpg" alt="">
                        </a>
                        <span class="product_tag position-absolute">15% OFF</span>
                        <span class="product_tag style2 position-absolute">Voucher: KFC 150</span>
                        <span class="date_tag position-absolute">
                            <h6>25</h6>
                            <p>MIN</p>
                        </span>
                    </div>
                    <div class="product_meta">
                        <h3><a href="product_details.php">Kosturi Kitchen Gulshan</a></h3>
                        <p> <span class="mark_text">$$$ </span>  Sushi <span class="dot_devide">•</span>   Japanese  <span class="dot_devide"></span>  Noodles  <span class="dot_devide">•</span>  Sashimi</p>
                        <div class="raing_box d-flex align-items-center">
                            <div class="raing_box_inner">
                                <span>4.9/5</span>
                                <i class="fas fa-star"></i>
                            </div>
                            <span  class="rating_text" >( 110 Rating )</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                <div class="product_wized mb_30">
                    <div class="thumb position-relative">
                        <a href="product_details.php" class="d-block">
                            <img src="img/product/product1.jpg" alt="">
                        </a>
                        <span class="product_tag position-absolute">15% OFF</span>
                        <span class="product_tag style2 position-absolute">Voucher: KFC 150</span>
                        <span class="date_tag position-absolute">
                            <h6>25</h6>
                            <p>MIN</p>
                        </span>
                    </div>
                    <div class="product_meta">
                        <h3><a href="product_details.php">Kosturi Kitchen Gulshan</a></h3>
                        <p> <span class="mark_text">$$$ </span>  Sushi <span class="dot_devide">•</span>   Japanese  <span class="dot_devide"></span>  Noodles  <span class="dot_devide">•</span>  Sashimi</p>
                        <div class="raing_box d-flex align-items-center">
                            <div class="raing_box_inner">
                                <span>4.9/5</span>
                                <i class="fas fa-star"></i>
                            </div>
                            <span  class="rating_text" >( 110 Rating )</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                <div class="product_wized mb_30">
                    <div class="thumb position-relative">
                        <a href="product_details.php" class="d-block">
                            <img src="img/product/product1.jpg" alt="">
                        </a>
                        <span class="product_tag position-absolute">15% OFF</span>
                        <span class="product_tag style2 position-absolute">Voucher: KFC 150</span>
                        <span class="date_tag position-absolute">
                            <h6>25</h6>
                            <p>MIN</p>
                        </span>
                    </div>
                    <div class="product_meta">
                        <h3><a href="product_details.php">Kosturi Kitchen Gulshan</a></h3>
                        <p> <span class="mark_text">$$$ </span>  Sushi <span class="dot_devide">•</span>   Japanese  <span class="dot_devide"></span>  Noodles  <span class="dot_devide">•</span>  Sashimi</p>
                        <div class="raing_box d-flex align-items-center">
                            <div class="raing_box_inner">
                                <span>4.9/5</span>
                                <i class="fas fa-star"></i>
                            </div>
                            <span  class="rating_text" >( 110 Rating )</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mb_25">
            <div class="col-12">
                <div class="section__title mb_27">
                    <h3 class="heading fs-4 ">Popular Restaurants</h3>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                <div class="product_wized mb_30">
                    <div class="thumb position-relative">
                        <a href="product_details.php" class="d-block">
                            <img src="img/product/product1.jpg" alt="">
                        </a>
                        <span class="product_tag position-absolute">15% OFF</span>
                        <span class="product_tag style2 position-absolute">Voucher: KFC 150</span>
                        <span class="date_tag position-absolute">
                            <h6>25</h6>
                            <p>MIN</p>
                        </span>
                    </div>
                    <div class="product_meta">
                        <h3><a href="product_details.php">Kosturi Kitchen Gulshan</a></h3>
                        <p> <span class="mark_text">$$$ </span>  Sushi <span class="dot_devide">•</span>   Japanese  <span class="dot_devide"></span>  Noodles  <span class="dot_devide">•</span>  Sashimi</p>
                        <div class="raing_box d-flex align-items-center">
                            <div class="raing_box_inner">
                                <span>4.9/5</span>
                                <i class="fas fa-star"></i>
                            </div>
                            <span  class="rating_text" >( 110 Rating )</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                <div class="product_wized mb_30">
                    <div class="thumb position-relative">
                        <a href="product_details.php" class="d-block">
                            <img src="img/product/product1.jpg" alt="">
                        </a>
                        <span class="product_tag position-absolute">15% OFF</span>
                        <span class="product_tag style2 position-absolute">Voucher: KFC 150</span>
                        <span class="date_tag position-absolute">
                            <h6>25</h6>
                            <p>MIN</p>
                        </span>
                    </div>
                    <div class="product_meta">
                        <h3><a href="product_details.php">Kosturi Kitchen Gulshan</a></h3>
                        <p> <span class="mark_text">$$$ </span>  Sushi <span class="dot_devide">•</span>   Japanese  <span class="dot_devide"></span>  Noodles  <span class="dot_devide">•</span>  Sashimi</p>
                        <div class="raing_box d-flex align-items-center">
                            <div class="raing_box_inner">
                                <span>4.9/5</span>
                                <i class="fas fa-star"></i>
                            </div>
                            <span  class="rating_text" >( 110 Rating )</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                <div class="product_wized mb_30">
                    <div class="thumb position-relative">
                        <a href="product_details.php" class="d-block">
                            <img src="img/product/product1.jpg" alt="">
                        </a>
                        <span class="product_tag position-absolute">15% OFF</span>
                        <span class="product_tag style2 position-absolute">Voucher: KFC 150</span>
                        <span class="date_tag position-absolute">
                            <h6>25</h6>
                            <p>MIN</p>
                        </span>
                    </div>
                    <div class="product_meta">
                        <h3><a href="product_details.php">Kosturi Kitchen Gulshan</a></h3>
                        <p> <span class="mark_text">$$$ </span>  Sushi <span class="dot_devide">•</span>   Japanese  <span class="dot_devide"></span>  Noodles  <span class="dot_devide">•</span>  Sashimi</p>
                        <div class="raing_box d-flex align-items-center">
                            <div class="raing_box_inner">
                                <span>4.9/5</span>
                                <i class="fas fa-star"></i>
                            </div>
                            <span  class="rating_text" >( 110 Rating )</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                <div class="product_wized mb_30">
                    <div class="thumb position-relative">
                        <a href="product_details.php" class="d-block">
                            <img src="img/product/product1.jpg" alt="">
                        </a>
                        <span class="product_tag position-absolute">15% OFF</span>
                        <span class="product_tag style2 position-absolute">Voucher: KFC 150</span>
                        <span class="date_tag position-absolute">
                            <h6>25</h6>
                            <p>MIN</p>
                        </span>
                    </div>
                    <div class="product_meta">
                        <h3><a href="product_details.php">Kosturi Kitchen Gulshan</a></h3>
                        <p> <span class="mark_text">$$$ </span>  Sushi <span class="dot_devide">•</span>   Japanese  <span class="dot_devide"></span>  Noodles  <span class="dot_devide">•</span>  Sashimi</p>
                        <div class="raing_box d-flex align-items-center">
                            <div class="raing_box_inner">
                                <span>4.9/5</span>
                                <i class="fas fa-star"></i>
                            </div>
                            <span  class="rating_text" >( 110 Rating )</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mb_70 ">
            <div class="col-12">
                <div class="section__title mb_27">
                    <h3 class="heading fs-4 ">Popular Restaurants</h3>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                <div class="product_wized mb_30">
                    <div class="thumb position-relative">
                        <a href="product_details.php" class="d-block">
                            <img src="img/product/product1.jpg" alt="">
                        </a>
                        <span class="product_tag position-absolute">15% OFF</span>
                        <span class="product_tag style2 position-absolute">Voucher: KFC 150</span>
                        <span class="date_tag position-absolute">
                            <h6>25</h6>
                            <p>MIN</p>
                        </span>
                    </div>
                    <div class="product_meta">
                        <h3><a href="product_details.php">Kosturi Kitchen Gulshan</a></h3>
                        <p> <span class="mark_text">$$$ </span>  Sushi <span class="dot_devide">•</span>   Japanese  <span class="dot_devide"></span>  Noodles  <span class="dot_devide">•</span>  Sashimi</p>
                        <div class="raing_box d-flex align-items-center">
                            <div class="raing_box_inner">
                                <span>4.9/5</span>
                                <i class="fas fa-star"></i>
                            </div>
                            <span  class="rating_text" >( 110 Rating )</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                <div class="product_wized mb_30">
                    <div class="thumb position-relative">
                        <a href="product_details.php" class="d-block">
                            <img src="img/product/product1.jpg" alt="">
                        </a>
                        <span class="product_tag position-absolute">15% OFF</span>
                        <span class="product_tag style2 position-absolute">Voucher: KFC 150</span>
                        <span class="date_tag position-absolute">
                            <h6>25</h6>
                            <p>MIN</p>
                        </span>
                    </div>
                    <div class="product_meta">
                        <h3><a href="product_details.php">Kosturi Kitchen Gulshan</a></h3>
                        <p> <span class="mark_text">$$$ </span>  Sushi <span class="dot_devide">•</span>   Japanese  <span class="dot_devide"></span>  Noodles  <span class="dot_devide">•</span>  Sashimi</p>
                        <div class="raing_box d-flex align-items-center">
                            <div class="raing_box_inner">
                                <span>4.9/5</span>
                                <i class="fas fa-star"></i>
                            </div>
                            <span  class="rating_text" >( 110 Rating )</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                <div class="product_wized mb_30">
                    <div class="thumb position-relative">
                        <a href="product_details.php" class="d-block">
                            <img src="img/product/product1.jpg" alt="">
                        </a>
                        <span class="product_tag position-absolute">15% OFF</span>
                        <span class="product_tag style2 position-absolute">Voucher: KFC 150</span>
                        <span class="date_tag position-absolute">
                            <h6>25</h6>
                            <p>MIN</p>
                        </span>
                    </div>
                    <div class="product_meta">
                        <h3><a href="product_details.php">Kosturi Kitchen Gulshan</a></h3>
                        <p> <span class="mark_text">$$$ </span>  Sushi <span class="dot_devide">•</span>   Japanese  <span class="dot_devide"></span>  Noodles  <span class="dot_devide">•</span>  Sashimi</p>
                        <div class="raing_box d-flex align-items-center">
                            <div class="raing_box_inner">
                                <span>4.9/5</span>
                                <i class="fas fa-star"></i>
                            </div>
                            <span  class="rating_text" >( 110 Rating )</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                <div class="product_wized mb_30">
                    <div class="thumb position-relative">
                        <a href="product_details.php" class="d-block">
                            <img src="img/product/product1.jpg" alt="">
                        </a>
                        <span class="product_tag position-absolute">15% OFF</span>
                        <span class="product_tag style2 position-absolute">Voucher: KFC 150</span>
                        <span class="date_tag position-absolute">
                            <h6>25</h6>
                            <p>MIN</p>
                        </span>
                    </div>
                    <div class="product_meta">
                        <h3><a href="product_details.php">Kosturi Kitchen Gulshan</a></h3>
                        <p> <span class="mark_text">$$$ </span>  Sushi <span class="dot_devide">•</span>   Japanese  <span class="dot_devide"></span>  Noodles  <span class="dot_devide">•</span>  Sashimi</p>
                        <div class="raing_box d-flex align-items-center">
                            <div class="raing_box_inner">
                                <span>4.9/5</span>
                                <i class="fas fa-star"></i>
                            </div>
                            <span  class="rating_text" >( 110 Rating )</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 text-center">
                <a href="#" class="theme_line_btn shadow_btn max_btn">Load more Restaurent</a>
            </div>
        </div>
    </div>
</div>
<!-- infix_popular_area::end  -->

<!-- infix_subscribe_area::start  -->
<div class="infix_subscribe_area">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-9">
                <div class="infix_subscribe_box">
                    <div class="infix_subscribe_text">
                        <h3>Get started for free!</h3>
                        <p>Order lunch, fuel for meetings or late-night deliveries 
                            your favorite restaurants desk near you.</p>
                    </div>
                    <div class="infix_subscribe_form">
                        <input class="infix_primary_input" type="text" placeholder="Type e-mail address">
                        <button class="black_btn shadow_btn width_160">Get started</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- infix_subscribe_area::end  -->


<?php include 'include/footer_content2.php' ?>
<?php include 'include/footer.php' ?>