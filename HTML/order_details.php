<?php include 'include/header.php' ?>
<?php include 'include/menu.php' ?>

<!-- order_details_area::start  -->
<div class="order_details_area section_spacing6">
    <div class="container">
        <div class="row">
            <div class="col-xl-4 mb_30 offset-xl-3 col-md-6">
                <div class="order_details_Estimated">
                    <div class="order_details_Estimated_box text-center mb_20">
                        <p class="estimated_text font_14 f_w_500">Estimated Delivery Time</p>
                        <h3>26 - 36 Minutes</h3>
                        <img class="img-fluid" src="img/cooking_img.png" alt="">
                        <div class="cooking_animated">
                            <span class="amimated_pils"></span>
                            <span class="amimated_pils"></span>
                            <span class="amimated_pils"></span>
                            <span class="amimated_pils"></span>
                        </div>
                        <p class="preparing_text font_14 f_w_500">Preparing your food. Your rider will pick 
                        it up once it’s ready.</p>
                    </div>
                    <div class="rider_chat_box d-flex align-items-center gap-2" >
                        <div class="rider_chat_box_text flex-fill">
                            <h3 class="fs-4 f_w_700">Contact to Rider.</h3>
                            <p class="font_14 f_w_400 mb-0">Ask your rider to contactless delivery.</p>
                        </div>
                        <a href="#" data-bs-toggle="modal" data-bs-target="#catting_modal">
                            <img src="img/chatting.svg" alt="">
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 mb_30 col-md-6">
                <div class="order_product_details mb_20">
                    <h3 class="fs-4 fw-bold mb_30">Product Details</h3>
                    <div class="op_details">
                        <div class="single_dsc">
                            <span class="text_1 font_14 f_w_400">Your Order Form:</span>
                            <span class="text_2 fs-6 fw-bold">Takeout - Manhattan</span>
                        </div>
                        <div class="single_dsc">
                            <span class="text_1 font_14 f_w_400">Order Number:</span>
                            <span class="text_2 fs-6 fw-bold">#6YRDZMD04</span>
                        </div>
                        <div class="single_dsc">
                            <span class="text_1 font_14 f_w_400">Delivery Address: </span>
                            <span class="text_2 fs-6 fw-bold">2593 Timbercrest Road, USA</span>
                        </div>
                    </div>
                    <ul class="op_details_list">
                        <li>
                            <div class="op_details_list_single">
                                <span class="d_text font_14"> <span class="fs-6 f_w_700 mr_10">2</span> x    Chicken Satay</span>
                                <p>+ USD 324.35</p>
                            </div>
                        </li>
                        <li>
                            <div class="op_details_list_single Subtotal">
                                <span class="d_text f_w_500 font_16"> Subtotal</span>
                                <p class="f_w_400 font_14">+ USD 1324.35</p>
                            </div>
                            <div class="op_details_list_single">
                                <span class="d_text f_w_500 font_16"> Delivery Fee</span>
                                <p class="f_w_400 font_14">+ USD 75.35</p>
                            </div>
                        </li>
                        <li>
                            <div class="op_details_list_single d-flex justify-content-between align-items-center Total_text">
                                <h4 class="font_14 f_w_700 mb-0">Total (Incl. VAT)</h4>
                                <h4 class="font_14 f_w_700 mb-0">+ USD 1324.35</h4>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="rider_chat_box " >
                    <h3 class="fs-4 f_w_700">Need Support?</h3>
                    <p class="font_14 f_w_400 mb_15">Question regarding to your Order? Reach out to us.</p>
                    <a href="#" class="theme_line_btn style4 fw-bold text-capitalize">Help Center</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- order_details_area::end  -->


<!-- infix_subscribe_area::start  -->
<div class="infix_subscribe_area">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-9">
                <div class="infix_subscribe_box">
                    <div class="infix_subscribe_text">
                        <h3>Get started for free!</h3>
                        <p>Order lunch, fuel for meetings or late-night deliveries 
                            your favorite restaurants desk near you.</p>
                    </div>
                    <div class="infix_subscribe_form">
                        <input class="infix_primary_input" type="text" placeholder="Type e-mail address">
                        <button class="black_btn shadow_btn width_160">Get started</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- infix_subscribe_area::end  -->
<?php include 'include/footer_content2.php' ?>
<?php include 'include/footer.php' ?>