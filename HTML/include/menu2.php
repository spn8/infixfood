<body class="home_one">
    <!-- preloader  -->
    <div class="preloader" >
        <h3 data-text="InfixFood..">InfixFood..</h3>
    </div>
    <!-- preloader:end  -->

<!-- HEADER::START -->
<header>
    <div id="sticky-header" class="header_area_one">
        <!-- main_header_area  -->
        <div class="main_header_area">
            <div class="shop_header_wrapper d-flex align-items-center">
                <div class="menu_logo">
                    <a href="index.php">
                        <img src="img/logo.png" alt="">
                    </a>
                </div>
                <div class="header_wrapper_right">
                    <div class="language_text single_header_info">
                        <span class="lang_opt">EN</span>
                        <span class="vertical_line"></span>
                        <span class="lang_opt">BN</span>
                    </div>
                    <div class="admin_use_info single_header_info d-flex align-items-center">
                        <div class="admin_use_info_inner d-flex align-items-center">
                            <div class="thumb">
                                <img src="img/login_icon.png" alt="">
                            </div>
                            <h4 class="m-0">Login</h4>
                        </div>
                    </div>
                    <span class="single_wishcart_lists single_header_info add_to_cart">
                        <div class="icon m-0">
                            <img src="img/cart.svg" alt="">
                        </div>
                    </span>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="mobile_menu d-block d-lg-none"></div>
                </div>
            </div>
        </div>
    </div>
</header>
<!--/ HEADER::END -->