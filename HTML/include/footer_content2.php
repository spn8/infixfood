    <!-- FOOTER::START  -->
    <footer class="home_one_footer">
        <div class="main_footer_wrap">
            <div class="container">
                 <div class="row">
                    <div class="col-xl-3 col-lg-3 col-md-6">
                        <div class="footer_widget" >
                            <div class="footer_title">
                                <h3>Get Help Form Us</h3>
                            </div>
                            <ul class="footer_links">
                                <li><a href="profile.php">Account Details</a></li>
                                <li><a href="my_order.php">Order History</a></li>
                                <li><a href="product.php">Find restaurant</a></li>
                                <li><a href="deliverd.php">Track order</a></li>
                                <li><a href="index2.php">Home two</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-6">
                        <div class="footer_widget" >
                            <div class="footer_title">
                                <h3>Company</h3>
                            </div>
                            <ul class="footer_links">
                                <li><a href="faq.php">Terms and conditions</a></li>
                                <li><a href="#">Download Mobile Apps</a></li>
                                <li><a href="faq.php">Account balance policy</a></li>
                                <li><a href="faq.php">Partner with us</a></li>
                                <li><a href="faq.php">Careers</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3  col-md-6">
                        <div class="footer_widget" >
                            <div class="footer_title">
                                <h3>Support</h3>
                            </div>
                            <ul class="footer_links">
                                <li><a href="faq.php">Help Center</a></li>
                                <li><a href="profile.php">Corporate Customer</a></li>
                                <li><a href="faq.php">Contact Us</a></li>
                                <li><a href="faq.php">support@infixfood.com</a></li>
                                <li><a href="search_page.php">search page</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3  col-md-6">
                        <div class="footer_widget" >
                            <div class="social__Links">
                                <a href="#">
                                    <i class="fab fa-facebook"></i>
                                </a>
                                <a class="twitter" href="#">
                                    <i class="fab fa-twitter"></i>
                                </a>
                                <a class="instagram" href="#">
                                    <i class="fab fa-instagram"></i>
                                </a>
                                 <a class="linkedin" href="#">
                                    <i class="fab fa-linkedin-in"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright_area">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 d-flex align-items-center flex-wrap gap-2">
                        <div class="copy_right_text flex-fill">
                            <p>© 2020 infixfood. All rights reserved. Design and Developed by <a href="#">SpondonIT</a>.</p>
                        </div>
                        <div class="country_lists">
                            <a href="#">England</a>
                            <a href="#">Bangladesh</a>
                            <a href="#">Japan</a>
                            <a href="#">Thailand</a>
                            <a href="#">Singapore</a>
                            <a href="#">Canada</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- FOOTER::END  -->

