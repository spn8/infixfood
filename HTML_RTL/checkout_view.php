<?php include 'include/header.php' ?>
<?php include 'include/menu.php' ?>

<!-- faq_area::start  -->
<div class="faq_area section_spacing4 ">
    <div class="container">
        <!-- content  -->
        <div class="row justify-content-center">
            <div class="col-xl-5 col-lg-7">
                <div class="checkout_according mb_30" id="accordion1">
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                            <button class="btn btn-link text_white" data-bs-toggle="collapse" data-bs-target="#collapseFour" aria-expanded="true" aria-controls="collapseFour">1. Delivery Details</button>
                            </h5>
                        </div>
                        <div class="collapse show" id="collapseFour" aria-labelledby="headingFour" data-parent="#accordion1">
                            <div class="card-body">
                                <div class="checout_body_info">
                                    <div class="address_quote mb_25 d-flex align-items-center gap-3 flex-wrap position-relative">
                                        <div class="address_quote_left flex-fill">
                                            <h3 class="font_18 f_w_700 mb-1">Contactless delivery</h3>
                                            <p class="font_14 f_w_400 mb-0 ">The rider will place your order at your door</p>
                                        </div>
                                        <label class="lmsSwitch_toggle" for="Anable">
                                            <input type="checkbox" id="Anable">
                                            <div class="slider round"></div>
                                        </label>
                                    </div>
                                    <h4 class="font_16 f_w_500 mb_15">Delivery time :</h4>
                                    <div class="row ">
                                        <div class="col-xxl-6">
                                            <select class="theme_select nice-select mb_25 wide" >
                                                <option value="1">Thu, Dec 20</option>
                                                <option value="1">Thu, Dec 20</option>
                                            </select>
                                        </div>
                                        <div class="col-xxl-6">
                                            <select class="theme_select nice-select mb_25 wide" >
                                                <option value="1">ASAP</option>
                                                <option value="1">ASAP</option>
                                            </select>
                                        </div>
                                    </div>
                                    <h4 class="font_16 f_w_500 mb_25">Delivery Address :</h4>
                                    <div class="address_box_view position-relative">
                                        <h4 class="font_18 f_w_700 mb-0">2593 Timbercrest Road, Chisana, USA</h4>
                                        <p class="font_14 f_w_400"><span class="f_w_500">Note to Rider:</span> Hurryup</p>
                                        <span class="addcart_action">
                                            <i class="ti-plus"></i>
                                            <span>Add to Cart</span>
                                        </span>
                                        <div class="address_box_right">
                                            <a href="#">
                                                <i class="ti-new-window"></i>
                                            </a>
                                            <a href="#">
                                            <i class="ti-trash"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                            <h5 class="mb-0">
                            <button class="btn btn-link collapsed text_white" data-bs-toggle="collapse" data-bs-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">2. Personal Details</button>
                            </h5>
                        </div>
                        <div class="collapse" id="collapseFive" aria-labelledby="headingFive" data-parent="#accordion1">
                            <div class="card-body">
                                <h3 class="font_18 f_w_700 ">Robert Downey JR.</h3>
                                <p class="font_14 f_w_400 mb-0">spn12@spondonit.com</p>
                                <p class="font_14 f_w_400 mb-0">+880 1754272664</p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingSix">
                            <h5 class="mb-0">
                            <button class="btn btn-link collapsed text_white" data-bs-toggle="collapse" data-bs-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">3. Payment</button>
                            </h5>
                        </div>
                        <div class="collapse " id="collapseSix" aria-labelledby="headingSix" data-parent="#accordion1">
                            <div class="card-body p-0">
                                <div class="checout_body_payment">
                                    <p class="text_1 mb_25">No payment is required for the current order. Please press on <span>“Place order” </span>
                                    to complete your order.</p>
                                    <button class="theme_line_btn font_14 fw-bold text-capitalize d-flex align-items-center gap_10 style5 mb_15"> <i class="ti-plus font_16"></i>  <span>Add Voucher</span> </button>
                                    <p class="text_2 mb_35">By making this purchase you agree to our <a href="#">terms and conditions.</a></p>
                                    <button class="theme_btn btn_50 w-100 text-center text-uppercase shadow_btn mb_20">Place order</button>
                                    <p class="text_3">I agree that placing the order places me under an obligation to make a payment in 
                                        accordance with the General Terms and Conditions.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3">
                <div class="check_product_details mb_30 p-0 border-0 shadow-none">
                    <h3 class="font_18 fw-bold mb_20">Product Details</h3>
                    <ul class="op_details_list">
                        <li>
                            <div class="op_details_list_single">
                                <span class="d_text font_14"> <span class="fs-6 f_w_700 mr_13">2</span> x    Chicken Satay</span>
                                <p>+ USD 324.35</p>
                            </div>
                            <div class="op_details_list_single">
                                <span class="d_text font_14"> <span class="fs-6 f_w_700 mr_13">2</span> x    Chicken Satay</span>
                                <p>+ USD 324.35</p>
                            </div>
                        </li>
                        <li>
                            <div class="op_details_list_single Subtotal">
                                <h5 class="f_w_500 font_16 mb-0"> Subtotal</h5>
                                <p class="f_w_400 font_14">+ USD 1324.35</p>
                            </div>
                            <div class="op_details_list_single">
                                <h5 class="f_w_500 font_16 mb-0"> Delivery Fee</h5>
                                <p class="f_w_400 font_14">+ USD 75.35</p>
                            </div>
                            <div class="op_details_list_single">
                                <h5 class="f_w_500 font_16 mb-0"> Discount</h5>
                                <p class="f_w_400 font_14">+ USD 206.35</p>
                            </div>
                        </li>
                        <li>
                            <div class="op_details_list_single d-flex justify-content-between align-items-center Total_text">
                                <h4 class="font_14 f_w_700 mb-0">Total (Incl. VAT)</h4>
                                <h4 class="font_14 f_w_700 mb-0">+ USD 1324.35</h4>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!--/ content  -->
    </div>
</div>
<!-- faq_area::end  -->


<!-- infix_subscribe_area::start  -->
<div class="infix_subscribe_area">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-9">
                <div class="infix_subscribe_box">
                    <div class="infix_subscribe_text">
                        <h3>Get started for free!</h3>
                        <p>Order lunch, fuel for meetings or late-night deliveries 
                            your favorite restaurants desk near you.</p>
                    </div>
                    <div class="infix_subscribe_form">
                        <input class="infix_primary_input" type="text" placeholder="Type e-mail address">
                        <button class="black_btn shadow_btn width_160">Get started</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- infix_subscribe_area::end  -->


<?php include 'include/footer_content2.php' ?>
<?php include 'include/footer.php' ?>