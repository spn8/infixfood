<body class="home_one">
    <!-- preloader  -->
    <div class="preloader" >
        <h3 data-text="InfixFood..">InfixFood..</h3>
    </div>
    <!-- preloader:end  -->

<!-- HEADER::START -->
<header>
    <div id="sticky-header" class="header_area_one">
        <!-- main_header_area  -->
        <div class="main_header_area">
            <div class="shop_header_wrapper d-flex align-items-center">
                <div class="menu_logo">
                    <a href="index.php">
                        <img src="img/logo.png" alt="">
                    </a>
                </div>
                <div class="header_wrapper_right position-relative">
                    <div class="delivery_address_text single_header_info position-relative d-none d-xl-flex">
                        <span class="delevering_text">DELIVERING TO: </span>
                        <span class="delevering_add">Kazi Nazrul Islam Avenue … <i class="ti-angle-down" ></i></span>
                        <div class="address_serach_box ">
                            <div class="input-group theme_search_field shadow-none">
                                <input type="text" class="form-control border-end-0" placeholder="Enter your full address" aria-label="Enter your full address" aria-describedby="basic-addon2">
                                <span class="input-group-text border-start-0" id="basic-addon2"><img src="img/svgs/loc_icon.svg" alt=""></span>
                            </div>
                            <button class="addressBtn">
                                <img src="img/svgs/right-arrow.svg " alt="">
                            </button>
                        </div>
                    </div>
                    <div class="deliver_timing single_header_info d-none d-lg-flex">
                        <span class="timng_text">When:</span>
                        <span class="timng_asp">ASAP</span>
                    </div>
                    <div class="language_text single_header_info d-none d-sm-flex">
                        <span class="lang_opt">EN</span>
                        <span class="vertical_line"></span>
                        <span class="lang_opt">BN</span>
                    </div>
                    <div class="admin_use_info single_header_info d-flex align-items-center position-relative">
                        <div class="admin_use_info_inner d-flex align-items-center">
                            <div class="thumb">
                                <img src="img/user.png" alt="">
                            </div>
                            <h4 class="m-0">Percy Jackson</h4>
                            <i class="ti-angle-down"></i>
                        </div>
                        <div class="admin_dropdown position-absolute">
                            <h4 class="m-0 font_16 f_w_700 d-md-none">Percy Jackson</h4>
                            <div class="seperator d-md-none"></div>
                            <ul>
                                <li><a href="my_order.php">My Orders</a></li>
                                <li><a href="profile.php">Profile</a></li>
                                <li><a href="vouchers.php">Vouchers</a></li>
                                <li><a href="refund.php">Refund Account</a></li>
                            </ul>
                            <div class="seperator"></div>
                            <ul>
                                <li><a href="faq.php">Help Center</a></li>
                                <li><a href="#">Logout</a></li>
                            </ul>
                        </div>
                    </div>
                    <span  class="single_wishcart_lists single_header_info add_to_cart">
                        <div class="icon m-0">
                            <img src="img/cart.svg" alt="">
                        </div>
                    </span>
                    
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="mobile_menu d-block d-lg-none"></div>
                </div>
            </div>
        </div>
    </div>
</header>
<!--/ HEADER::END -->