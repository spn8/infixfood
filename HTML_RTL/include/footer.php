

    <!-- about:start  -->
    <div class="modal fade login_modal about_modal" id="asq_about_form" tabindex="-1" role="dialog" aria-labelledby="login_form" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                <div data-bs-dismiss="modal" class="close_modal">
                    <i class="ti-close"></i>
                </div>
                <!-- infix_login_area::start  -->
                    <div class="infix_login_area p-0">
                        <div class="login_area_inner">
                            <h3 class="sign_up_text mb_20 fs-5">Have A question?</h3>
                            <form action="#">
                                <div class="row">
                                    <div class="col-md-12">
                                        <textarea placeholder="Your Message" class="primary_textarea3 mb_20 bg-white" ></textarea>
                                    </div>
                                    <div class="col-md-12">
                                        <input placeholder="Your Name" type="text" class="primary_input3 mb_20 bg-white">
                                    </div>
                                    <div class="col-md-12">
                                        <input placeholder="Email" type="email" class="primary_input3 mb_20 bg-white">
                                    </div>
                                    <div class="col-md-12">
                                        <input placeholder="Your Phone" type="text" class="primary_input3 mb_30 bg-white">
                                    </div>
                                    <div class="col-12">
                                        <button class="home10_primary_btn2 text-center f_w_700">SEND MESSAGE</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- infix_login_area::end  -->
                    
                </div>
            </div>
        </div>
    </div>
    <!-- about:end  -->

    <!-- invite_modal:start  -->
    <div class="modal fade invite_modal modal_570px" id="invite_modal" tabindex="-1" role="dialog" aria-labelledby="login_form" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content border-0  rounded-0 ">
                <div class="modal-body">
                    <div data-bs-dismiss="modal" class="close_modal">
                        <i class="ti-close"></i>
                    </div>
                    <div class="invitation_form">
                        <h3 class=" font_20 f_w_700 mb_20" >Invitation Link</h3>
                        <input type="text" class="infix_primary_input shadow-none style2" placeholder="https://infixfood.page.link/USv4UzhEjmxKDP5UA">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- invite_modal:end  -->

    <!-- catting_modal:start  -->
    <div class="modal fade theme_modal modal_810px" id="catting_modal" tabindex="-1" role="dialog" aria-labelledby="login_form" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content border-0  rounded-0 ">
                <div class="modal-body p-0">
                    <!-- catting_wrapper  -->
                    <div class="catting_wrapper">
                        <div class="catting_wrapper_header d-flex align-items-center flex-wrap">
                            <h3 class="flex-fill font_20 f_w_700 m-0">Rider Contact</h3>
                            <img src="img/svgs/translate.svg" alt="">
                            <span data-bs-dismiss="modal" class="close_modal">
                                <i class="ti-close"></i>
                            </span>
                        </div>
                        <div class="catting_wrapper_body">
                            <div class="single_catting_box mb_30 large_img">
                                <p class="font_14 f_w_400  mb_20">Request your rider to simply eave your order at the door. Message your rider 
                                    with drop off instructions for contractless delivery.</p>
                                <h5 class="font_14 f_w_500  mb-2">Delivery Details:</h5>
                                <p class="font_14 f_w_400">2593 Timbercrest Road, Chisana, USA</p>
                            </div>
                            <div class="single_catting_box mb_30 reply_img">
                                <p class="font_14 f_w_400 text-white mb_15">Request your rider to simply eave your order at the door 
                                    Message your rider with drop off instructions.</p>
                                <div class="deliverd_time d-flex align-items-center justify-content-between">
                                    <span class="font_14 f_w_500 text-white m-0">16.45</span>
                                    <img src="img/svgs/double_checkIcon.svg " alt="">
                                </div>
                            </div>
                            <div class="single_catting_box mb_30 ">
                                <p class="font_14 f_w_400  mb_15">Request your rider to simply eave your order at the door 
                                    Message your rider with drop off instructions.</p>
                                <div class="deliverd_time d-flex align-items-center justify-content-between">
                                    <span class="font_14 f_w_500  m-0">16.45</span>
                                </div>
                            </div>
                            <div class="single_catting_box mb_30 reply_img">
                                <p class="font_14 f_w_400 text-white mb_15">Request your rider to simply eave your order at the door 
                                    Message your rider with drop off instructions.</p>
                                <div class="deliverd_time d-flex align-items-center justify-content-between">
                                    <span class="font_14 f_w_500 text-white m-0">16.45</span>
                                    <img src="img/svgs/double_checkIcon.svg " alt="">
                                </div>
                            </div>
                        </div>
                        <div class="catting_wrapper_footer">
                            <div class="input-group m-0">
                                <input type="text" class="form-control shadow-none" placeholder="Send a Reply to Rider!" aria-label="Send a Reply to Rider!" aria-describedby="button-addon2">
                                <button type="button" id="button-addon2">
                                    <img src="img/svgs/plane_img.svg" alt="">
                                </button>
                            </div>
                        </div>
                    </div>
                    <!-- catting_wrapper  -->
                </div>
            </div>
        </div>
    </div>
    <!-- catting_modal:end  -->


    <!-- invite_modal:start  -->
    <div class="modal fade infix_foodMOdal modal_930px" id="about_modal" tabindex="-1" role="dialog" aria-labelledby="about_modal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content border-0  rounded-0 ">
                <div class="modal-body">
                    <div class="modal_bg"></div>
                    <div data-bs-dismiss="modal" class="close_modal">
                        <i class="ti-close"></i>
                    </div>
                    <div class="about_modal_content">
                        <!-- about_modal_head::start  -->
                        <div class="about_modal_head">
                            <div class="about_modal_title">
                                <h3 class="d-flex align-items-center  gap_10 flex-wrap ">
                                    <a href="product_details.php">Kosturi Kitchen Gulshan</a>
                                </h3>
                                <div class="raing_box d-flex align-items-center">
                                    <div class="raing_box_inner">
                                        <span>4.9/5</span>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <span class="rating_text">( 110 Rating )</span>
                                </div>
                            </div>
                            <p> <span class="mark_text">$$$ </span>  Sushi <span class="dot_devide">•</span>   Japanese  <span class="dot_devide">•</span>  Noodles  <span class="dot_devide">•</span>  Sashimi</p>
                            <h5 class="fs-6 f_w_500 text-center mb_30">Open 9:00 AM - 10:59 PM</h5>
                            <ul class="nav review_nav_tab" id="myTab" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link active" id="about_tab-tab" data-bs-toggle="tab" data-bs-target="#about_tab" type="button" role="tab" aria-controls="home" aria-selected="true">About</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="review_tab-tab" data-bs-toggle="tab" data-bs-target="#review_tab" type="button" role="tab" aria-controls="profile" aria-selected="false">Reviews</button>
                                </li>
                            </ul>
                        </div>
                        <!-- about_modal_head::end  -->
                        <div class="about_review_tab">
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="about_tab" role="tabpanel" aria-labelledby="about_tab-tab">
                                    <!-- content ::start  -->
                                    <div class="office_info">
                                        <div class="office_info_left">
                                            <div class="single_office_address">
                                                <h4>Delivery Hours</h4>
                                                <p>Mon - Sun 9:00 AM - 10:59 PM</p>
                                            </div>
                                            <div class="single_office_address">
                                                <h4>Address Line</h4>
                                                <p>House No. 67, Kalabagan Bus Stand, Dhaka</p>
                                            </div>
                                        </div>
                                        <div class="office_info_right">
                                            <div id="contact-map"></div>
                                        </div>
                                    </div>
                                    <!-- content ::end -->
                                </div>
                                <div class="tab-pane fade" id="review_tab" role="tabpanel" aria-labelledby="review_tab-tab">
                                    <!-- content ::start  -->
                                    <div class="review_wrapper_wiz">
                                        <div class="details_title">
                                            <h3 class="f_s_20 f_w_700">60 Reviews</h3>
                                            <div class="border_1px border-top-0 border-start-0 border-end-0"> </div>
                                        </div>
                                        <!-- customers_reviews  -->
                                        <div class="customers_reviews">
                                            <!-- single_reviews  -->
                                            <div class="single_reviews">
                                                <div class="review_content">
                                                    <div class="review_content_head d-flex justify-content-between align-items-start flex-wrap">
                                                        <div class="review_content_head_left">
                                                            <h4 class="f_w_700 font_18">Kristen Stewart</h4>
                                                            <div class="rated_customer d-flex align-items-center">
                                                                <div class="feedmak_stars">
                                                                    <i class="fas fa-star"></i>
                                                                    <i class="fas fa-star"></i>
                                                                    <i class="fas fa-star"></i>
                                                                    <i class="fas fa-star"></i>
                                                                    <i class="fas fa-star"></i>
                                                                </div>
                                                                <span>2 weeks ago</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <p>See-through delicate embroidered organza blue lining luxury acetate-mix stretch pleat detailing. Leather detail shoulder contrastic colour contour stunni silhouette working peplum. Statement buttons cover-up tweaks patch pockets perennia lapel collar.</p>
                                                </div>
                                            </div>
                                            <!-- single_reviews  -->
                                            <div class="single_reviews">
                                                <div class="review_content">
                                                    <div class="review_content_head d-flex justify-content-between align-items-start flex-wrap">
                                                        <div class="review_content_head_left">
                                                            <h4 class="f_w_700 font_18">Kristen Stewart</h4>
                                                            <div class="rated_customer d-flex align-items-center">
                                                                <div class="feedmak_stars">
                                                                    <i class="fas fa-star"></i>
                                                                    <i class="fas fa-star"></i>
                                                                    <i class="fas fa-star"></i>
                                                                    <i class="fas fa-star"></i>
                                                                    <i class="fas fa-star"></i>
                                                                </div>
                                                                <span>2 weeks ago</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <p>See-through delicate embroidered organza blue lining luxury acetate-mix stretch pleat detailing. Leather detail shoulder contrastic colour contour stunni silhouette working peplum. Statement buttons cover-up tweaks patch pockets perennia lapel collar.</p>
                                                </div>
                                            </div>
                                            <!-- single_reviews  -->
                                            <div class="single_reviews">
                                                <div class="review_content">
                                                    <div class="review_content_head d-flex justify-content-between align-items-start flex-wrap">
                                                        <div class="review_content_head_left">
                                                            <h4 class="f_w_700 font_18">Kristen Stewart</h4>
                                                            <div class="rated_customer d-flex align-items-center">
                                                                <div class="feedmak_stars">
                                                                    <i class="fas fa-star"></i>
                                                                    <i class="fas fa-star"></i>
                                                                    <i class="fas fa-star"></i>
                                                                    <i class="fas fa-star"></i>
                                                                    <i class="fas fa-star"></i>
                                                                </div>
                                                                <span>2 weeks ago</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <p>See-through delicate embroidered organza blue lining luxury acetate-mix stretch pleat detailing. Leather detail shoulder contrastic colour contour stunni silhouette working peplum. Statement buttons cover-up tweaks patch pockets perennia lapel collar.</p>
                                                </div>
                                            </div>
                                            <!-- single_reviews  -->
                                            <div class="single_reviews">
                                                <div class="review_content">
                                                    <div class="review_content_head d-flex justify-content-between align-items-start flex-wrap">
                                                        <div class="review_content_head_left">
                                                            <h4 class="f_w_700 font_18">Kristen Stewart</h4>
                                                            <div class="rated_customer d-flex align-items-center">
                                                                <div class="feedmak_stars">
                                                                    <i class="fas fa-star"></i>
                                                                    <i class="fas fa-star"></i>
                                                                    <i class="fas fa-star"></i>
                                                                    <i class="fas fa-star"></i>
                                                                    <i class="fas fa-star"></i>
                                                                </div>
                                                                <span>2 weeks ago</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <p>See-through delicate embroidered organza blue lining luxury acetate-mix stretch pleat detailing. Leather detail shoulder contrastic colour contour stunni silhouette working peplum. Statement buttons cover-up tweaks patch pockets perennia lapel collar.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex justify-content-center">
                                        <button class="theme_line_btn style2 text-center">Load more Reviews</button>
                                    </div>
                                    <!-- content ::end -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- invite_modal:end  -->

    <!-- voucher_modal:start  -->
    <div class="modal fade modal_570px voucher_modal" id="voucher_modal" tabindex="-1" role="dialog" aria-labelledby="voucher_modal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content border-0  rounded-0 ">
                <div class="modal-body">
                    <div data-bs-dismiss="modal" class="close_modal">
                        <i class="ti-close"></i>
                    </div>
                    <h3 class="font_20 f_w_700 mb_10">Enter or Select a Voucher</h3>
                    <div class="voucher_lists mb_20">
                        <div class="voucher_list_single d-flex align-items-center gap_25 flex-wrap">
                            <div class="voucher_list_left d-flex align-items-center gap_15">
                                <label class="round_checkbox blue_check  d-flex mr_15">
                                    <input name="color_filt" type="checkbox">
                                    <span class="checkmark"></span>
                                </label>
                                <h4 class="font_16 f_w_500 mb-0">I have a Voucher</h4>
                            </div>
                            <div class="voucher_list_right flex-fill">
                                <input type="text" class="primary_input style3 shadow-none" placeholder="Enter your voucher">
                            </div>
                        </div>
                        <div class="voucher_list_single d-flex align-items-start gap_25">
                            <div class="voucher_list_left d-flex  gap_15 flex-fill">
                                <label class="round_checkbox blue_check  d-flex mr_15">
                                    <input name="color_filt" type="checkbox">
                                    <span class="checkmark"></span>
                                </label>
                                <div class="voucher_list_info">
                                    <h4 class="font_16 f_w_500 mb-0">Take Your First Bite</h4>
                                    <p class="font_14 f_w_400 mb-0">Expires: 2020-12-23</p>
                                    <p class="font_14 f_w_400 mb-0">Minimum order: USD 120.00</p>
                                </div>
                            </div>
                            <div class="voucher_list_right text-end">
                                <span class="font_14 f_w_400 mb-0 secondary_text">+ USD 324.35</span>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex justify-content-end">
                        <button class="theme_btn btn_50 shadow_btn width_160">Apply</button>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <!-- voucher_modal:end  -->


    <!-- cart_modal:start  -->
    <div class="modal fade infix_cartModal modal_690px" id="cart_modal" tabindex="-1" role="dialog" aria-labelledby="cart_modal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content border-0  rounded-0 ">
                <div class="modal-body">
                    <div data-bs-dismiss="modal" class="close_modal">
                        <i class="ti-close"></i>
                    </div>
                    <div class="modal_title mb_15">
                        <h3 class="fs-4 f_w_700 d-flex align-items-center gap_10 mb_6 flex-wrap">Kosturi Kitchen Gulshan <span class="font_14 f_w_400 mb-0 secondary_text">BDT 324.35</span></h3>
                        <p class="font_14 f_w_400">Prepared with cheese, chicken, bbq sauce, mushroom, tomato & capsicum</p>
                    </div>
                    <div class="variation_wrapper">
                        <div class="single_variation">
                            <div class="variation_title d-flex align-items-center gap_20 flex-wrap mb_20">
                                <h4 class="font_20 f_w_700 m-0">Select Variation  </h4>
                                <span class="badge_btn">1 REQUIRED</span>
                            </div>
                            <ul class="variation_list">
                                <li class="d-flex align-items-center justify-content-between">
                                    <div class="d-flex align-items-center gap_15">
                                        <label class="round_checkbox2 blue_check  d-flex mr_15">
                                            <input name="color_filt" type="checkbox">
                                            <span class="checkmark"></span>
                                        </label>
                                        <h4 class="mb-0 font_18 f_w_700">6”</h4>
                                    </div>
                                    <p class="font_14 f_w_400">+ USD 324.35</p>
                                </li>
                                <li class="d-flex align-items-center justify-content-between">
                                    <div class="d-flex align-items-center gap_15">
                                        <label class="round_checkbox2 blue_check  d-flex mr_15">
                                            <input name="color_filt" type="checkbox">
                                            <span class="checkmark"></span>
                                        </label>
                                        <h4 class="mb-0 font_18 f_w_700">8”</h4>
                                    </div>
                                    <p class="font_14 f_w_400">+ USD 324.35</p>
                                </li>
                                <li class="d-flex align-items-center justify-content-between">
                                    <div class="d-flex align-items-center gap_15">
                                        <label class="round_checkbox2 blue_check  d-flex mr_15">
                                            <input name="color_filt" type="checkbox">
                                            <span class="checkmark"></span>
                                        </label>
                                        <h4 class="mb-0 font_18 f_w_700">10”</h4>
                                    </div>
                                    <p class="font_14 f_w_400">+ USD 324.35</p>
                                </li>
                                <li class="d-flex align-items-center justify-content-between">
                                    <div class="d-flex align-items-center gap_15">
                                        <label class="round_checkbox2 blue_check  d-flex mr_15">
                                            <input name="color_filt" type="checkbox">
                                            <span class="checkmark"></span>
                                        </label>
                                        <h4 class="mb-0 font_18 f_w_700">12”</h4>
                                    </div>
                                    <p class="font_14 f_w_400">+ USD 324.35</p>
                                </li>
                            </ul>
                        </div>
                        <div class="single_variation">
                            <div class="variation_title d-flex align-items-center gap_20 flex-wrap mb-0">
                                <h4 class="font_20 f_w_700 m-0">Select Variation  </h4>
                                <span class="badge_btn style2">Optional</span>
                            </div>
                            <p class="font_14 f_w_400 lh-1 mb_20">Select up to 8 (optional)</p>
                            <ul class="variation_list mb_20 variation_list_collaspe">
                                <li class="d-flex align-items-center justify-content-between">
                                    <div class="d-flex align-items-center gap_15">
                                        <label class="primary_checkbox2 d-flex">
                                            <input type="checkbox">
                                            <span class="checkmark"></span>
                                        </label>
                                        <h4 class="mb-0 font_16 f_w_500">Extra Cheese</h4>
                                    </div>
                                    <p class="font_14 f_w_400">+ USD 324.35</p>
                                </li>
                                <li class="d-flex align-items-center justify-content-between">
                                    <div class="d-flex align-items-center gap_15">
                                        <label class="primary_checkbox2 d-flex">
                                            <input type="checkbox">
                                            <span class="checkmark"></span>
                                        </label>
                                        <h4 class="mb-0 font_16 f_w_500">Extra Naga</h4>
                                    </div>
                                    <p class="font_14 f_w_400">+ USD 324.35</p>
                                </li>
                                <li class="d-flex align-items-center justify-content-between">
                                    <div class="d-flex align-items-center gap_15">
                                        <label class="primary_checkbox2 d-flex">
                                            <input type="checkbox">
                                            <span class="checkmark"></span>
                                        </label>
                                        <h4 class="mb-0 font_16 f_w_500">Extra Mushroom</h4>
                                    </div>
                                    <p class="font_14 f_w_400">+ USD 324.35</p>
                                </li>
                                <li class="d-flex align-items-center justify-content-between">
                                    <div class="d-flex align-items-center gap_15">
                                        <label class="primary_checkbox2 d-flex">
                                            <input type="checkbox">
                                            <span class="checkmark"></span>
                                        </label>
                                        <h4 class="mb-0 font_16 f_w_500">Extra Sausage</h4>
                                    </div>
                                    <p class="font_14 f_w_400">+ USD 324.35</p>
                                </li>
                                <li class="d-flex align-items-center justify-content-between">
                                    <div class="d-flex align-items-center gap_15">
                                        <label class="primary_checkbox2 d-flex">
                                            <input type="checkbox">
                                            <span class="checkmark"></span>
                                        </label>
                                        <h4 class="mb-0 font_16 f_w_500">Extra Naga</h4>
                                    </div>
                                    <p class="font_14 f_w_400">+ USD 324.35</p>
                                </li>
                                <li class="d-flex align-items-center justify-content-between">
                                    <div class="d-flex align-items-center gap_15">
                                        <label class="primary_checkbox2 d-flex">
                                            <input type="checkbox">
                                            <span class="checkmark"></span>
                                        </label>
                                        <h4 class="mb-0 font_16 f_w_500">Extra Mushroom</h4>
                                    </div>
                                    <p class="font_14 f_w_400">+ USD 324.35</p>
                                </li>
                                <li class="d-flex align-items-center justify-content-between">
                                    <div class="d-flex align-items-center gap_15">
                                        <label class="primary_checkbox2 d-flex">
                                            <input type="checkbox">
                                            <span class="checkmark"></span>
                                        </label>
                                        <h4 class="mb-0 font_16 f_w_500">Extra Sausage</h4>
                                    </div>
                                    <p class="font_14 f_w_400">+ USD 324.35</p>
                                </li>
                            </ul>
                            <span class="secondary_text d-flex align-items-center gap_20 viewMore_text">
                                <i class="ti-angle-down font_12"></i>
                                <span class="font_14 f_w_500 text_toglle gj-cursor-pointer">View More (03)</span>
                            </span>
                        </div>
                    </div>
                    <div class="Instructions_info ">
                        <div class="row">
                            <div class="col-12">
                                <h4 class="font_20 f_w_700 mb-0">Special Instructions</h4>
                                <p class="font_14 f_w_400 mb_15">Any specific preferences? Let the restaurant know.</p>
                                <textarea class="primary_textarea mb_40 height_120" placeholder="Write your special instructions"></textarea>
                                <h4 class="font_20 f_w_700 mb_20">If this Product is not Available</h4>
                            </div>
                            <div class="col-12">
                                <select class="theme_select nice-select mb_30 wide ">
                                    <option value="1">Remove it from my order</option>
                                    <option value="1">Remove it from my order</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="cart_btn_group d-flex align-items-center  gap_30 flex-wrap">
                    <div class="product_number_count mr_5" data-target="amount-1">
                        <span class="count_single_item inumber_decrement"> <i class="ti-minus"></i></span>
                        <input id="amount-1" class="count_single_item input-number" type="text" value="1">
                        <span class="count_single_item number_increment"> <i class="ti-plus"></i></span>
                    </div>
                    <button class="theme_btn text-center flex-fill">Add to cart</button>
                </div>
            </div>
        </div>
    </div>
    <!-- cart_modal:end  -->
    <div class="serch_oveflay"></div>

    <!-- shoping_cart::start  -->
    <div class="shoping_wrapper">
        <!-- <div class="dark_overlay"></div> -->
        <div class="shoping_cart cart_scroll">
            <div class="shoping_cart_inner p-0">
                <!-- shoping_cart_inner_top  -->
                <div class="shoping_cart_inner_top">
                    <div class="delevary_cart d-flex align-items-center gap_15 mb_15">
                        <img src="img/svgs/car.svg" alt="">
                        <h4 class="font_14 f_w_500 m-0">60 Min to deliver</h4>
                    </div>
                    <div class="cart_header d-flex justify-content-between">
                        <h4>Shopping Cart</h4>
                        <div class="chart_close">
                            <i class="ti-close"></i>
                        </div>
                    </div>
                    <div class="single_cart flex-column">
                        <div class="cart_title d-flex align-items-center justify-content-between">
                            <h4 class="font_18 f_w_700 m-0">Kosturi Kitchen Gulshan</h4>
                            <span class="secondary_text font_14  m-0 text-nowrap">+ USD 324.35</span>
                        </div>
                        <div class="cart_counts d-flex align-items-center gap_20">
                            <div class="product_number_count mr_5" data-target="amount-31">
                                <span class="count_single_item inumber_decrement"> <i class="ti-minus"></i></span>
                                <input id="amount-31" class="count_single_item input-number" type="text" value="1">
                                <span class="count_single_item number_increment"> <i class="ti-plus"></i></span>
                            </div>
                            <div class="size_text d-flex align-items-center gap-1">
                                <p class="font_14 f_w_400 text-uppercase m-0">Size:</p>
                                <h4 class="font_16 f_w_500 m-0">Regular</h4>
                            </div>
                        </div>
                    </div>
                    <div class="single_cart flex-column">
                        <div class="cart_title d-flex align-items-center justify-content-between">
                            <h4 class="font_18 f_w_700 m-0">Kosturi Kitchen Gulshan</h4>
                            <span class="secondary_text font_14  m-0 text-nowrap">+ USD 324.35</span>
                        </div>
                        <div class="cart_counts d-flex align-items-center gap_20">
                            <div class="product_number_count mr_5" data-target="amount-23">
                                <span class="count_single_item inumber_decrement"> <i class="ti-minus"></i></span>
                                <input id="amount-23" class="count_single_item input-number" type="text" value="1">
                                <span class="count_single_item number_increment"> <i class="ti-plus"></i></span>
                            </div>
                            <div class="size_text d-flex align-items-center gap-1">
                                <p class="font_14 f_w_400 text-uppercase m-0">Size:</p>
                                <h4 class="font_16 f_w_500 m-0">Regular</h4>
                            </div>
                        </div>
                    </div>
                    <div class="single_cart flex-column">
                        <div class="cart_title d-flex align-items-center justify-content-between">
                            <h4 class="font_18 f_w_700 m-0">Kosturi Kitchen Gulshan</h4>
                            <span class="secondary_text font_14  m-0 text-nowrap">+ USD 324.35</span>
                        </div>
                        <div class="cart_counts d-flex align-items-center gap_20">
                            <div class="product_number_count mr_5" data-target="amount-13">
                                <span class="count_single_item inumber_decrement"> <i class="ti-minus"></i></span>
                                <input id="amount-13" class="count_single_item input-number" type="text" value="1">
                                <span class="count_single_item number_increment"> <i class="ti-plus"></i></span>
                            </div>
                            <div class="size_text d-flex align-items-center gap-1">
                                <p class="font_14 f_w_400 text-uppercase m-0">Size:</p>
                                <h4 class="font_16 f_w_500 m-0">Regular</h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="popular_orders_acrousel ">
                    <h3 class="font_20 f_w_700 mb_20">Popular with your Order</h3>
                    <div class="owl-carousel popular_order_active">
                        <div class="product_d_wized2 position-relative">
                            <h4 class="m-0 d-flex align-items-center justify-content-between gap-1 flex-wrap"><a  href="#">Sausage Lover’s Pizza</a> <span class="prise ">USD 324.35</span></h4>
                            <p> Mid Budget Pizza</p>
                            <span class="addcart_action" data-bs-target="#cart_modal" data-bs-toggle="modal">
                                <i class="ti-plus"></i>
                                <span>Add to Cart</span>
                            </span>
                        </div>
                        <div class="product_d_wized2 position-relative">
                            <h4 class="m-0 d-flex align-items-center justify-content-between gap-1 flex-wrap"><a  href="#">Sausage Lover’s Pizza</a> <span class="prise ">USD 324.35</span></h4>
                            <p> Mid Budget Pizza</p>
                            <span class="addcart_action" data-bs-target="#cart_modal" data-bs-toggle="modal">
                                <i class="ti-plus"></i>
                                <span>Add to Cart</span>
                            </span>
                        </div>
                        <div class="product_d_wized2 position-relative">
                            <h4 class="m-0 d-flex align-items-center justify-content-between gap-1 flex-wrap"><a  href="#">Sausage Lover’s Pizza</a> <span class="prise ">USD 324.35</span></h4>
                            <p> Mid Budget Pizza</p>
                            <span class="addcart_action" data-bs-target="#cart_modal" data-bs-toggle="modal">
                                <i class="ti-plus"></i>
                                <span>Add to Cart</span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="shoping_cart_inner_bottom ">
                    <div class="total_cart_amount">
                        <div class="single_total_cart_amount d-flex align-items-center justify-content-between">
                            <h4 class="font_16 f_w_500 m-0">Subtotal</h4>
                            <span class="font_14 f_w_400 m-0">+ USD 1324.35</span>
                        </div>
                        <div class="single_total_cart_amount d-flex align-items-center justify-content-between">
                            <h4 class="font_16 f_w_500 m-0">Delivery Fee</h4>
                            <span class="font_14 f_w_400 m-0">+ USD 75.35</span>
                        </div>
                        <div class="single_total_cart_amount d-flex align-items-center justify-content-between">
                            <h4 class="font_16 f_w_500 m-0">Discount</h4>
                            <span class="font_14 f_w_400 m-0">+ USD 206.35</span>
                        </div>
                        <div class="single_total_cart_amount d-flex align-items-center justify-content-between Total_amount_exp">
                            <h4 class="font_14 f_w_700 m-0">Total (Incl. VAT)</h4>
                            <h4 class="font_14 f_w_700 m-0">Total (Incl. VAT)</h4>
                        </div>
                    </div>
                    <a href="checkout.php" class="theme_btn w-100 text-center">Go to checkout</a>
                </div>
            </div>
        </div>
    </div>
    <!-- shoping_cart::end  -->


    <!-- UP_ICON  -->
    <div id="back-top" style="display: none;">
        <a title="Go to Top" href="#">
            <i class="ti-angle-up"></i>
        </a>
    </div>
    <!--/ UP_ICON -->

    <!--ALL JS SCRIPTS -->
    <script src="js/vendor/jquery-3.4.1.min.js"></script>
    <script src="js/vendor/popper.min.js"></script>
    <script src="js/vendor/bootstrap.min.js"></script>
    
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/isotope.pkgd.min.js"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="js/jquery.counterup.min.js"></script>
    <script src="js/imagesloaded.pkgd.min.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/nice-select.min.js"></script>
    <script src="js/barfiller.js"></script>
    <script src="js/jquery.slicknav.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/jquery.ajaxchimp.min.js"></script>
    <script src="js/parallax.js"></script>
     <script src="js/gijgo.min.js"></script>
     <script src="js/slick.min.js"></script>
     <script src="js/eleveti_zoom.js"></script>
     <script src="js/perfect-scrollbar.js"></script>
     <script src="js/jquery.nav.js"></script>
    <?php if (basename($_SERVER['PHP_SELF']) != 'apply_job.php') {?>
    <script src="js/query-ui.js"></script>
    <?php }?>
    <script src="js/jquery.countdown.min.js"></script>
    <script src="js/mail-script.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDfpGBFn5yRPvJrvAKoGIdj1O1aO9QisgQ"></script>
    <script src="js/map.js"></script>
    <!-- MAIN JS   -->
    <script src="js/main.js"></script>

</body>

</html>