<?php include 'include/header.php' ?>
<?php include 'include/menu.php' ?>

<!-- banner::start  -->
<div class="banner_one">
    <div class="banner_single banner_bg_1">
        <div class="container">
            <div class="row">
                <div class="col-xl-7">
                    <div class="banner__text">
                        <span class="f_w_500 text-uppercase">Explore the whole cities</span>
                        <h3>Hello! Honey… <br>
                            We don’t cook today.</h3>
                        <div class="banner_btns d-flex flex-wrap gap_10 align-items-center">
                            <div class="input-group theme_search_field">
                                <input type="text" class="form-control border-end-0" placeholder="Enter your full address" aria-label="Enter your full address" aria-describedby="basic-addon2">
                                <span class="input-group-text border-start-0" id="basic-addon2"><img src="img/svgs/loc_icon.svg" alt=""></span>
                            </div>
                            <button class="theme_btn shadow_btn">Find your favorites</button>
                        </div>
                        <p>Get the app and choose from 10,000+ restaurants in 70+ cities.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--/ banner::end  -->
<!-- infix_feature_area::start  -->
<div class="infix_feature_area section_spacing">
    <div class="container">
        <div class="row align-items-center" >
            <div class="col-xl-6">
                <div class="featuire_thumb position-relative mb_30">
                    <img class="w-100" src="img/home_1/1.jpg" alt="">
                    <div class="feature_icon position-absolute top-50 start-100 translate-middle">
                        <img src="img/svgs/about_icon.svg" alt="">
                    </div>
                </div>
            </div>
            <div class="col-xl-5 offset-xl-1 ">
                <div class="section__title mb_30">
                    <span class="sub_heading ">List your restaurant on infixshop</span>
                    <h3 class="heading">Award - winning 
                        app for delivery.</h3>
                    <p>Vivamus lacinia tempus rutrum nulla velit lupus maximus sednu
                        llaquis gravida sed tellus nibh metus euismod risus curabiturma
                        urisivamus lacinia tempus rutrum nulla velit.</p>
                    <a href="product.php" class="theme_btn">Get started now</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- infix_feature_area::end  -->
<!-- infix_popular_area::start  -->
<div class="infix_popular_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section__title mb_55 text-center">
                    <span class="sub_heading ">Find us in these cities and many more!</span>
                    <h3 class="heading">Popular Right Now</h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                <div class="popular_items_wized mb_55">
                    <a href="product_details.php" class="thumb d-block overflow-hidden">
                        <img class="img-fluid" src="img/product/1.jpg" alt="">
                    </a>
                    <div class="product_meta">
                        <a href="product_details.php">
                            <h3>Los Angeles, California</h3>
                        </a>
                        <p>Vivamus lacinia tempus rutrum nulla 
                                velit lupus maximus sednu.</p>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                <div class="popular_items_wized mb_55">
                    <a href="product_details.php" class="thumb d-block overflow-hidden">
                        <img class="img-fluid" src="img/product/2.jpg" alt="">
                    </a>
                    <div class="product_meta">
                        <a href="product_details.php">
                            <h3>Los Angeles, California</h3>
                        </a>
                        <p>Vivamus lacinia tempus rutrum nulla 
                                velit lupus maximus sednu.</p>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                <div class="popular_items_wized mb_55">
                    <a href="product_details.php" class="thumb d-block overflow-hidden">
                        <img class="img-fluid" src="img/product/3.jpg" alt="">
                    </a>
                    <div class="product_meta">
                        <a href="product_details.php">
                            <h3>Los Angeles, California</h3>
                        </a>
                        <p>Vivamus lacinia tempus rutrum nulla 
                                velit lupus maximus sednu.</p>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                <div class="popular_items_wized mb_55">
                    <a href="product_details.php" class="thumb d-block overflow-hidden">
                        <img class="img-fluid" src="img/product/4.jpg" alt="">
                    </a>
                    <div class="product_meta">
                        <a href="product_details.php">
                            <h3>Los Angeles, California</h3>
                        </a>
                        <p>Vivamus lacinia tempus rutrum nulla 
                                velit lupus maximus sednu.</p>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                <div class="popular_items_wized mb_55">
                    <a href="product_details.php" class="thumb d-block overflow-hidden">
                        <img class="img-fluid" src="img/product/5.jpg" alt="">
                    </a>
                    <div class="product_meta">
                        <a href="product_details.php">
                            <h3>Los Angeles, California</h3>
                        </a>
                        <p>Vivamus lacinia tempus rutrum nulla 
                                velit lupus maximus sednu.</p>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                <div class="popular_items_wized mb_55">
                    <a href="product_details.php" class="thumb d-block overflow-hidden">
                        <img class="img-fluid" src="img/product/6.jpg" alt="">
                    </a>
                    <div class="product_meta">
                        <a href="product_details.php">
                            <h3>Los Angeles, California</h3>
                        </a>
                        <p>Vivamus lacinia tempus rutrum nulla 
                                velit lupus maximus sednu.</p>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                <div class="popular_items_wized mb_55">
                    <a href="product_details.php" class="thumb d-block overflow-hidden">
                        <img class="img-fluid" src="img/product/7.jpg" alt="">
                    </a>
                    <div class="product_meta">
                        <a href="product_details.php">
                            <h3>Los Angeles, California</h3>
                        </a>
                        <p>Vivamus lacinia tempus rutrum nulla 
                                velit lupus maximus sednu.</p>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                <div class="popular_items_wized mb_55">
                    <a href="product_details.php" class="thumb d-block overflow-hidden">
                        <img class="img-fluid" src="img/product/8.jpg" alt="">
                    </a>
                    <div class="product_meta">
                        <a href="product_details.php">
                            <h3>Los Angeles, California</h3>
                        </a>
                        <p>Vivamus lacinia tempus rutrum nulla 
                                velit lupus maximus sednu.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- infix_popular_area::end  -->

<!-- infix_download_area::start  -->
<div class="infix_download_area download_bg_1">
    <div class="container">
        <div class="row">
            <div class="col-xl-6 col-lg-7">
                <div class="section__title ">
                    <span class="sub_heading ">Download the app now</span>
                    <h3 class="heading">Our App Available For
                Your Smartphone.</h3>
                    <p>Get the app and choose from 10,000+ restaurants in 70+ cities.</p>
                    <div class="download_btns">
                        <a href="#">
                            <img src="img/svgs/google-play.svg" alt="">
                            <div class="download_info">
                                <span>Get it on</span>
                                <h5 class="m-0">Google Play</h5>
                            </div>
                        </a>
                        <a href="#">
                            <img src="img/svgs/apple.svg" alt="">
                            <div class="download_info">
                                <span>Download on</span>
                                <h5 class="m-0">Apple Store</h5>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- infix_download_area::end  -->

<!-- infix_feature_area::start  -->
<div class="infix_feature_area section_spacing">
    <div class="container">
        <div class="row align-items-center" >
            <div class="col-xl-6 ">
                <div class="featuire_thumb position-relative mb_30">
                    <img class="w-100" src="img/home_1/2.jpg" alt="">
                    <div class="feature_icon position-absolute top-50 start-100 translate-middle">
                        <img src="img/svgs/about_icon.svg" alt="">
                    </div>
                </div>
            </div>
            <div class="col-xl-5 offset-xl-1  ">
                <div class="section__title mb_30 full_width">
                    <span class="sub_heading ">List your restaurant on infixshop</span>
                    <h3 class="heading">Favorite restaurants 
                    coming to desk.</h3>
                    <p>Vivamus lacinia tempus rutrum nulla velit lupus maximus sednu
                        llaquis gravida sed tellus nibh metus euismod risus curabiturma
                        urisivamus lacinia tempus rutrum nulla velit.</p>
                    <a href="product.php" class="theme_btn">Get started now</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- infix_feature_area::end  -->

<!-- infix_popular_restaurant::start  -->
<div class="infix_popular_restaurant ">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section__title mb_30 text-center">
                    <span class="sub_heading ">POPULAR RESTAURANTS IN TOWN</span>
                    <h3 class="heading">Popular Cuisines</h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xxl-2 col-xl-3 col-lg-3 col-md-4">
                <div class="restaurant_wiz mb_30">
                    <a href="product_details.php" class="thumb d-block overflow-hidden">
                        <img src="img/restaurants/1.jpg" alt="">
                    </a>
                    <a href="product_details.php">
                        <h3>Mexican Food</h3>
                    </a>
                </div>
            </div>
            <div class="col-xxl-2 col-xl-3 col-lg-3 col-md-4">
                <div class="restaurant_wiz mb_30">
                    <a href="product_details.php" class="thumb d-block overflow-hidden">
                        <img src="img/restaurants/2.jpg" alt="">
                    </a>
                    <a href="product_details.php">
                        <h3>Chinese Food</h3>
                    </a>
                </div>
            </div>
            <div class="col-xxl-2 col-xl-3 col-lg-3 col-md-4">
                <div class="restaurant_wiz mb_30">
                    <a href="product_details.php" class="thumb d-block overflow-hidden">
                        <img src="img/restaurants/3.jpg" alt="">
                    </a>
                    <a href="product_details.php">
                        <h3>Vegetarian Food</h3>
                    </a>
                </div>
            </div>
            <div class="col-xxl-2 col-xl-3 col-lg-3 col-md-4">
                <div class="restaurant_wiz mb_30">
                    <a href="product_details.php" class="thumb d-block overflow-hidden">
                        <img src="img/restaurants/4.jpg" alt="">
                    </a>
                    <a href="product_details.php">
                        <h3>Italian Food</h3>
                    </a>
                </div>
            </div>
            <div class="col-xxl-2 col-xl-3 col-lg-3 col-md-4">
                <div class="restaurant_wiz mb_30">
                    <a href="product_details.php" class="thumb d-block overflow-hidden">
                        <img src="img/restaurants/5.jpg" alt="">
                    </a>
                    <a href="product_details.php">
                        <h3>Bakery & Cake</h3>
                    </a>
                </div>
            </div>
            <div class="col-xxl-2 col-xl-3 col-lg-3 col-md-4">
                <div class="restaurant_wiz mb_30">
                    <a href="product_details.php" class="thumb d-block overflow-hidden">
                        <img src="img/restaurants/6.jpg" alt="">
                    </a>
                    <a href="product_details.php">
                        <h3>Healthy Food</h3>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- infix_popular_restaurant::end  -->

<!-- infix_subscribe_area::start  -->
<div class="infix_subscribe_area">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-9">
                <div class="infix_subscribe_box">
                    <div class="infix_subscribe_text">
                        <h3>Get started for free!</h3>
                        <p>Order lunch, fuel for meetings or late-night deliveries 
                            your favorite restaurants desk near you.</p>
                    </div>
                    <div class="infix_subscribe_form">
                        <input class="infix_primary_input" type="text" placeholder="Type e-mail address">
                        <button class="black_btn shadow_btn width_160">Get started</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- infix_subscribe_area::end  -->


<?php include 'include/footer_content.php' ?>
<?php include 'include/footer.php' ?>