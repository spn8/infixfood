<?php include 'include/header.php' ?>
<?php include 'include/menu2.php' ?>

<!-- infix_login_area::start  -->
<div class="infix_login_area">
    <div class="login_area_inner">
        <div class="logo_img text-center">
            <img src="img/logo.png" alt="">
        </div>
        <h4 class="text-center">Welcome! Create an 
            account within a minute.</h4>
        <div class="login_with_links">
            <a class="Facebook_bg" href="#"> <i class="ti-facebook"></i>Login with Facebook</a>
            <a class="twitter_bg" href="#"> <i class="ti-twitter-alt"></i> Login with google</a>
        </div>
        <p class="sign_up_text mb_20 pb-1">Or Register with Email Address</p>
        <form action="#">
            <div class="row">
                <div class="col-12">
                    <input name="name" placeholder="Enter Name" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Name'"  class="primary_line_input mb_20" required="" type="text">

                    <input name="email" placeholder="Type e-mail address" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{1,63}$"
                        onfocus="this.placeholder = ''" onblur="this.placeholder = 'Type e-mail address'" class="primary_line_input mb_10" required="" type="email">
                    <input name="password" placeholder="Enter password" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter password'"
                        class="primary_line_input mb-0" required="" type="password">
                    <input name="password" placeholder="Re-enter password" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Re-enter password'" class="primary_line_input mb_10" required="" type="password">
                </div>
                <div class="col-12">
                    <div class="remember_pass mb_55 justify-content-start">
                        <label class="primary_checkbox d-flex ">
                            <input checked="" type="checkbox">
                            <span class="checkmark mr_15"></span>
                        </label>
                        <p class="font_14 f_w_500 mb-0 check_text">By signing up, you agree to <a class="text_underline" href="#"> Terms of Service</a> and <a class="text_underline"  href="#">Privacy Policy.</a></p>
                    </div>
                </div>
                <div class="col-12">
                    <button class="theme_btn large_btn w-100 text-center f_w_700">Sign Up</button>
                </div>
                <div class="col-12">
                    <p class="sign_up_text text-center" >Already have an account? <a href="login.php">Login</a></p>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- infix_login_area::end  -->

<?php include 'include/footer.php' ?>