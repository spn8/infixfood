<?php include 'include/header.php' ?>
<?php include 'include/menu.php' ?>

<!-- vouchers_area::start  -->
<div class="vouchers_area ">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xxl-4 col-xl-5 col-lg-6 col-md-8">
                <div class="voucher_title">
                    <h3 class="fs-4 mb_20 f_w_700 ">Vouchers</h3>
                </div>
                <div class="voucher_wrapper">
                    <div class="single_voucher d-flex flex-wrap gap_10 ">
                        <div class="voucher_content flex-fill">
                            <h4>Take Your First Bite</h4>
                            <p class="date_voucher">Expires: 2020-12-23</p>
                            <p class="order_text">Minimum order: USD 120.00</p>
                        </div>
                        <span class="prise_text">+ USD 324.35</span>
                    </div>
                    <div class="single_voucher d-flex flex-wrap gap_10 ">
                        <div class="voucher_content flex-fill">
                            <h4>Up to 50% OFF</h4>
                            <p class="date_voucher">Expires: 2020-12-23</p>
                            <p class="order_text">Minimum order: USD 120.00</p>
                        </div>
                        <span class="prise_text">+ USD 324.35</span>
                    </div>
                    <div class="single_voucher d-flex flex-wrap gap_10 ">
                        <div class="voucher_content flex-fill">
                            <h4>KHANA200</h4>
                            <p class="date_voucher">Expires: 2020-12-23</p>
                            <p class="order_text">Minimum order: USD 120.00</p>
                        </div>
                        <span class="prise_text">+ USD 324.35</span>
                    </div>
                    <div class="single_voucher d-flex flex-wrap gap_10 ">
                        <div class="voucher_content flex-fill">
                            <h4>BIJOY200 OFF</h4>
                            <p class="date_voucher">Expires: 2020-12-23</p>
                            <p class="order_text">Minimum order: USD 120.00</p>
                        </div>
                        <span class="prise_text">+ USD 324.35</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- vouchers_area::end  -->


<!-- infix_subscribe_area::start  -->
<div class="infix_subscribe_area">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-9">
                <div class="infix_subscribe_box">
                    <div class="infix_subscribe_text">
                        <h3>Get started for free!</h3>
                        <p>Order lunch, fuel for meetings or late-night deliveries 
                            your favorite restaurants desk near you.</p>
                    </div>
                    <div class="infix_subscribe_form">
                        <input class="infix_primary_input" type="text" placeholder="Type e-mail address">
                        <button class="black_btn shadow_btn width_160">Get started</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- infix_subscribe_area::end  -->


<?php include 'include/footer_content2.php' ?>
<?php include 'include/footer.php' ?>