<?php include 'include/header.php' ?>
<?php include 'include/menu.php' ?>

<!-- refund_area::start  -->
<div class="refund_area section_spacing4">
    <div class="container">
        <div class="row">
            <div class="col-xl-7 offset-xl-2 col-lg-7 offset-lg-2">
                <div class="refund_box flex-wrap">
                    <div class="refund_box_left">
                        <h3 class="fs-4 f_w_700 mb-3">Refund Account</h3>
                        <p class="font_14 mb-1">Balance</p>
                        <h4 class="fs-4 f_w_700 text-uppercase">USD 312.00</h4>
                    </div>
                    <div class="refund_info_box">
                        <h3 class="font_20 f_w_700 mb_20">Payment Methods</h3>
                        <div class="refund_info_inner">
                            <div class="refund_info_inner_left">
                                <p class="font_14 mb_15">Save a payment method at Checkout 
                                to view it right here.</p>
                                <a href="#" class="theme_btn small_btn6 text-capitalize">Add Restaurant</a>
                            </div>
                            <div class="refund_info_inner_right">
                                <img src="img/payment_img.png" class="img-fluid" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- refund_area::end  -->


<!-- infix_subscribe_area::start  -->
<div class="infix_subscribe_area">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-9">
                <div class="infix_subscribe_box">
                    <div class="infix_subscribe_text">
                        <h3>Get started for free!</h3>
                        <p>Order lunch, fuel for meetings or late-night deliveries 
                            your favorite restaurants desk near you.</p>
                    </div>
                    <div class="infix_subscribe_form">
                        <input class="infix_primary_input" type="text" placeholder="Type e-mail address">
                        <button class="black_btn shadow_btn width_160">Get started</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- infix_subscribe_area::end  -->
<?php include 'include/footer_content2.php' ?>
<?php include 'include/footer.php' ?>